package org.user5145.chocoshot.gateway.application.session

import io.ktor.application.*
import io.ktor.sessions.*
import io.ktor.util.*

fun Application.installSession() {
    install(Sessions) {
        cookie<Session>("SESSION", storage = SessionStorageMemory() ) {
            cookie.path = "/v1"
            cookie.maxAgeInSeconds = 60 * 60 * 24
            transform(SessionTransportTransformerMessageAuthentication(
                hex("CC92D2B69CEA6E259DF29B3AAA0EC0F151B06D800D65688AD6F04E81D16DC708E07A308C9A61E15035C4D12D8BE7C5A08EFB85F24153857D7B0C8643FFCF9CF5"),
                "HmacSHA256"))
        }
    }
}
