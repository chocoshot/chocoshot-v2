package org.user5145.chocoshot.gateway.application.versioning

import io.grpc.stub.StreamObserver
import org.user5145.chocoshot.gateway.application.registration.RegistrationServiceGrpcFactory
import org.user5145.chocoshot.grpc.ServerPort
import org.user5145.chocoshot.shared.rpc.*
import shared.Shared.Empty

class RegistrationService(
    private val service: RegistrationServiceGrpc.RegistrationServiceFutureStub = RegistrationServiceGrpcFactory.newInstance(ServerPort.REGISTRATION_SERVICE.value),
    private val proxy: GrpcProxyService = GrpcProxyService
): RegistrationServiceGrpc.RegistrationServiceImplBase()  {

    override fun register(request: RegisterAccountDTO, responseObserver: StreamObserver<Empty>) {
        proxy.proxyResult(responseObserver) { service.register(request).get() }
    }

    override fun getAccount(request: AccountRequest, responseObserver: StreamObserver<AccountDTO>) {
        proxy.proxyResult(responseObserver) { service.getAccount(request).get() }
    }

    override fun createChampion(request: CreateChampionDTO, responseObserver: StreamObserver<Empty>) {
        proxy.proxyResult(responseObserver) { service.createChampion(request).get() }
    }

    override fun getAccountsChampions(request: GetAccountsChampionsDTO, responseObserver: StreamObserver<ChampionsDTO>) {
        proxy.proxyResult(responseObserver) { service.getAccountsChampions(request).get() }
    }
}