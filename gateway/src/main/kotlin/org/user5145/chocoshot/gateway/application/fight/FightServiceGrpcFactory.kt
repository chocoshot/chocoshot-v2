package org.user5145.chocoshot.gateway.application.fight

import io.grpc.ManagedChannelBuilder
import org.user5145.chocoshot.shared.rpc.FightServiceGrpc

object FightServiceGrpcFactory {
    fun newInstance(port: Int = 2002, address: String = "localhost"): FightServiceGrpc.FightServiceFutureStub {
        val channel = ManagedChannelBuilder.forAddress(address, port)
            .usePlaintext()
            .build()
        return FightServiceGrpc.newFutureStub(channel)
    }
}