package org.user5145.chocoshot.gateway.application.fight

import io.ktor.application.*
import io.ktor.application.Application
import io.ktor.auth.*
import io.ktor.http.*
import io.ktor.http.ContentType.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.sessions.*
import mu.KotlinLogging
import org.user5145.chocoshot.gateway.application.session.Session
import org.user5145.chocoshot.json.extension.toJson

fun Application.fightController(fightServiceMapper: FightServiceMapper = FightServiceMapper()) {
    val logger = KotlinLogging.logger {}

    routing {
        route("/v1/fight") {
            authenticate("character") {
                get("/history") {
                    try {
                        val name = call.sessions.get<Session>()!!.character.get()
                        val response = fightServiceMapper.getHistory(name).toJson()
                        call.respondText(response, Text.Plain, HttpStatusCode.OK)
                    } catch (e: Exception) {
                        logger.warn(e.message)
                        call.respondText("Failed", Text.Plain, HttpStatusCode.Conflict)
                    }
                }

                put("/stats") {
                    try {
                        val parameters = call.receiveParameters()
                        val name = call.sessions.get<Session>()!!.character.get()
                        val hp = parameters["hp"]!!.toFloat()
                        val strength = parameters["strength"]!!.toFloat()
                        val response = fightServiceMapper.setStats(name, hp, strength).toJson()
                        call.respondText("ok", Text.Plain, HttpStatusCode.OK)
                    } catch (e: Exception) {
                        call.respondText("Failed", Text.Plain, HttpStatusCode.Conflict)
                    }
                }

                post("/attack") {
                    try {
                        val name = call.sessions.get<Session>()!!.character.get()
                        val response = fightServiceMapper.attack(name).toJson()
                        call.respondText(response, Text.Plain, HttpStatusCode.OK)
                    } catch (e: Exception) {
                        call.respondText("Failed", Text.Plain, HttpStatusCode.Conflict)
                    }
                }
            }

            authenticate("admin"){
                get("/enemies") {
                    try {
                        val response = fightServiceMapper.getEnemies().toJson()
                        call.respondText(response, Text.Plain, HttpStatusCode.OK)
                    } catch (e: Exception) {
                        call.respondText("Failed", Text.Plain, HttpStatusCode.Conflict)
                    }
                }

                delete("/enemies") {
                    try {
                        val parameters = call.receiveParameters()
                        val name = parameters["name"]!!
                        val response = fightServiceMapper.removeEnemy(name).toJson()
                        call.respondText("ok", Text.Plain, HttpStatusCode.OK)
                    } catch (e: Exception) {
                        call.respondText("Failed", Text.Plain, HttpStatusCode.Conflict)
                    }
                }

                post("/enemies") {
                    try {
                        val parameters = call.receiveParameters()
                        val name = parameters["name"]!!
                        val hpMin = parameters["hpMin"]!!.toInt()
                        val hpMax = parameters["hpMax"]!!.toInt()
                        val strengthMin = parameters["strengthMin"]!!.toInt()
                        val strengthMax = parameters["strengthMax"]!!.toInt()
                        val levelMin = parameters["levelMin"]!!.toInt()
                        val levelMax = parameters["levelMax"]!!.toInt()
                        val response = fightServiceMapper.addEnemy(name, hpMax, hpMin, levelMin, levelMax, strengthMin, strengthMax, ).toJson()
                        call.respondText("ok", Text.Plain, HttpStatusCode.OK)
                    } catch (e: Exception) {
                        call.respondText("Failed", Text.Plain, HttpStatusCode.Conflict)
                    }
                }
            }
        }
    }
}