package org.user5145.chocoshot.gateway.application.dto

data class ChampionProfileRestDto(val name: String)