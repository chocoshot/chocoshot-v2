package org.user5145.chocoshot.gateway.application.registration

import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.sessions.*
import mu.KotlinLogging
import org.user5145.chocoshot.gateway.application.bcrypt.BCryptService
import org.user5145.chocoshot.gateway.application.session.Session
import java.util.*

fun Application.installAuth(registrationServiceMapper: RegistrationServiceMapper = RegistrationServiceMapper()) {
    val logger = KotlinLogging.logger {}

    install(Authentication) {
        form("account") {
            enableAccountSignIn(registrationServiceMapper)
        }

        form("character") {
            enableChampionSignIn(registrationServiceMapper)
        }

        form("admin") {
            enableAdminSignIn(registrationServiceMapper)
        }
    }

    routing {
        authenticate("admin") {
            post("/v1/admin/login") {
                when {
                    call.sessions.get<Session>() != null ->
                        call.respond(status = HttpStatusCode.OK, message = "")

                    else -> {
                        val principal = call.authentication.principal<UserIdPrincipal>() ?: error("no auth found")
                        val login = principal.name
                        call.sessions.set(Session(account = login, character = Optional.empty(), admin=true))
                        call.respond(status = HttpStatusCode.OK, message = "")
                    }
                }
            }

            post("/v1/logout/admin") {
                runCatching {
                    call.sessions.clear<Session>()
                    call.respondText("OK", ContentType.Text.Plain, HttpStatusCode.OK)
                }
            }
        }

        authenticate("account") {
            post("/v1/login") {
                when {
                    call.sessions.get<Session>() != null ->
                        call.respond(status = HttpStatusCode.OK, message = "")

                    else -> {
                        val principal = call.authentication.principal<UserIdPrincipal>() ?: error("no auth found")
                        val login = principal.name
                        call.sessions.set(Session(account = login, character = Optional.empty()))
                        call.respond(status = HttpStatusCode.OK, message = "")
                    }
                }
            }

            post("/v1/logout") {
                runCatching {
                    call.sessions.clear<Session>()
                    call.respondText("OK", ContentType.Text.Plain, HttpStatusCode.OK)
                }
            }
        }

        authenticate("character") {
            post("/v1/log/character") {
                when {
                    call.sessions.get<Session>() == null ->
                        call.respond(status = HttpStatusCode.NonAuthoritativeInformation, message = "")

                    else -> {
                        val principal = call.authentication.principal<UserIdPrincipal>() ?: error("no auth found")
                        val session = call.sessions.get<Session>()!!
                        val character = Optional.of(principal.name)
                        session.character = character
                        call.respond(status = HttpStatusCode.OK, message = "")
                    }
                }
            }

            post("/v1/logout/character") {
                runCatching {
                    call.sessions.get<Session>()!!.character = Optional.empty()
                    call.respondText("OK", ContentType.Text.Plain, HttpStatusCode.OK)
                }
            }
        }
    }
}

fun FormAuthenticationProvider.Configuration.enableAccountSignIn(
    registrationServiceMapper: RegistrationServiceMapper,
    bcryptService: BCryptService = BCryptService()
){
    val logger = KotlinLogging.logger {}
    passwordParamName = "password"
    userParamName = "user"

    skipWhen {
        logger.debug { "validating session: ${it.sessions.get<Session>()?.account}" }
        return@skipWhen it.sessions.get<Session>() != null
    }

    validate { up: UserPasswordCredential ->
        logger.debug { "validating username: ${up.name}" }
        val account = registrationServiceMapper.getAccount(up.name)
        if ( bcryptService.validate(up.password, account.password) ){
            return@validate when (account.login) {
                up.name -> UserIdPrincipal(up.name)
                else -> null
            }
        } else {
            return@validate null
        }
    }
}

fun FormAuthenticationProvider.Configuration.enableAdminSignIn (
    registrationServiceMapper: RegistrationServiceMapper,
    bcryptService: BCryptService = BCryptService()
){
    val logger = KotlinLogging.logger {}
    passwordParamName = "password"
    userParamName = "user"

    skipWhen {
        logger.debug { "validating session: ${it.sessions.get<Session>()?.account}" }
        return@skipWhen it.sessions.get<Session>()?.admin ?: false
    }

    validate { up: UserPasswordCredential ->
        logger.debug { "validating admin: ${up.name}" }
        val account = registrationServiceMapper.getAccount(up.name)
        if ( bcryptService.validate(up.password, account.password) && account.isAdmin ){
            return@validate when (account.login) {
                up.name -> UserIdPrincipal(up.name)
                else -> null
            }
        } else {
            return@validate null
        }
    }
}

fun FormAuthenticationProvider.Configuration.enableChampionSignIn(registrationServiceMapper: RegistrationServiceMapper){
    val logger = KotlinLogging.logger {}
    passwordParamName = "password"
    userParamName = "user"

    skipWhen {
        logger.debug { "validating session: ${it.sessions.get<Session>()?.character?.get()}" }
        return@skipWhen it.sessions.get<Session>() != null
                && it.sessions.get<Session>()!!.character.isPresent
                && it.sessions.get<Session>()!!.account != null
    }

    validate { up: UserPasswordCredential ->
        logger.debug { "validating champion: ${up.name}" }
        val session = request.call.sessions.get<Session>() ?: return@validate null
        val account = session.account
        val champions = registrationServiceMapper.champions(account).champions
        if (champions.none { up.name == it.name }) {
            return@validate null
        } else {
            return@validate UserIdPrincipal(up.name)
        }
    }
}
