package org.user5145.chocoshot.gateway.application.dto

import com.google.common.collect.ImmutableList

data class GetQueueRestDto(val maxAvailable: Int, val list: ImmutableList<ActionDto>)


data class ActionDto(val id: Int, val timestamp: Long)