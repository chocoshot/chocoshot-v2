package org.user5145.chocoshot.gateway.application.dto

import org.user5145.chocoshot.shared.rpc.ChampionsDTO

object ChampionsProfileRestDtoFactory {
    fun create(dto: ChampionsDTO) = ChampionsProfileRestDto(dto.championsList.map { ChampionProfileRestDto(name = it.name) })
}