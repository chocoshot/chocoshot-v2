package org.user5145.chocoshot.gateway.application.dto

import com.google.common.collect.ImmutableList
import org.user5145.chocoshot.shared.rpc.Action
import org.user5145.chocoshot.shared.rpc.ActionQueue

object GetQueueRestDtoFactory {
    fun create(queue: ActionQueue) = GetQueueRestDto( queue.queueLimit, mapActionToActionDto(queue.actionsList) )

    private fun mapActionToActionDto(actionsList: MutableList<Action>): ImmutableList<ActionDto> =
        ImmutableList.copyOf(actionsList.map { ActionDto(it.id, it.timestamp ) })
}