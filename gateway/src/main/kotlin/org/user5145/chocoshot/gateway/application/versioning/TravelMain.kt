package org.user5145.chocoshot.gateway.application.versioning

import org.user5145.chocoshot.grpc.GrpcServer
import org.user5145.chocoshot.grpc.ServerPort

class TravelMain {
    fun main(){
        GrpcServer.start(TravelService(), ServerPort.TRAVEL_API_GATEWAY)
    }
}