package org.user5145.chocoshot.gateway.application.bcrypt

import at.favre.lib.crypto.bcrypt.BCrypt

class BCryptService {
    fun hash(pass: String): String {
        return BCrypt.withDefaults().hashToString(12, pass.toCharArray())
    }

    fun validate(pass1: String, pass2: String): Boolean {
        return BCrypt.verifyer(BCrypt.Version.VERSION_2A).verifyStrict(pass1.toCharArray(), pass2.toCharArray()).verified
    }
}