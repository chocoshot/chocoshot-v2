package org.user5145.chocoshot.gateway.application.dto

data class GetPlayerLocationRestDto(val x: Int, val y: Int)