package org.user5145.chocoshot.gateway.application.versioning

import org.user5145.chocoshot.grpc.GrpcServer
import org.user5145.chocoshot.grpc.ServerPort

class RegistrationMain {
    fun main(){
        GrpcServer.start(RegistrationService(), ServerPort.REGISTRATION_API_GATEWAY)
    }
}