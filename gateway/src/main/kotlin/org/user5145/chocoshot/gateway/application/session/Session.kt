package org.user5145.chocoshot.gateway.application.session

import java.util.*

data class Session(val account: String, var character: Optional<String>, val admin: Boolean = false)