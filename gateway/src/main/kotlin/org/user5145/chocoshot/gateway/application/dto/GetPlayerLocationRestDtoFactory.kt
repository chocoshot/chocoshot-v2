package org.user5145.chocoshot.gateway.application.dto

import org.user5145.chocoshot.shared.rpc.Coordinates

object GetPlayerLocationRestDtoFactory {
    fun create(position: Coordinates) = GetPlayerLocationRestDto(x = position.x, y = position.y)
}