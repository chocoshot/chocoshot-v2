package org.user5145.chocoshot.gateway.application.registration

import org.user5145.chocoshot.gateway.application.dto.ChampionsProfileRestDto
import org.user5145.chocoshot.gateway.application.dto.ChampionsProfileRestDtoFactory
import org.user5145.chocoshot.gateway.application.dto.LoginRestDtoFactory
import org.user5145.chocoshot.shared.rpc.*

class RegistrationServiceMapper(
    val registrationService: RegistrationServiceGrpc.RegistrationServiceFutureStub = RegistrationServiceGrpcFactory.newInstance(),
    val loginRestDtoFactory: LoginRestDtoFactory = LoginRestDtoFactory,
    val championProfileRestDtoFactory: ChampionsProfileRestDtoFactory = ChampionsProfileRestDtoFactory ) {

    fun createChampion(login: String, name: String) {
        val outgoingDto = CreateChampionDTO.newBuilder()
            .setLogin(login)
            .setName(name)
            .build()
        registrationService.createChampion(outgoingDto).get()
    }

    fun champions(login: String): ChampionsProfileRestDto {
        val outgoingDto = GetAccountsChampionsDTO.newBuilder()
            .setLogin(login)
            .build()
        val dto = registrationService.getAccountsChampions(outgoingDto).get()
        return championProfileRestDtoFactory.create(dto)
    }

    fun register(login: String, password: String){
        val dto = RegisterAccountDTO.newBuilder()
            .setLogin(login)
            .setPass(password)
            .build()
        val response = registrationService.register(dto).get()
    }

    fun getAccount(
        login: String): AccountDTO {
        val outgoingDto = AccountRequest.newBuilder()
            .setLogin(login)
            .build()
        val response = registrationService.getAccount(outgoingDto)
        return response.get()
    }
}
