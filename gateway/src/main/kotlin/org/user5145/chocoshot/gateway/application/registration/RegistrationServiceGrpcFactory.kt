package org.user5145.chocoshot.gateway.application.registration

import io.grpc.ManagedChannelBuilder
import org.user5145.chocoshot.shared.rpc.RegistrationServiceGrpc

object RegistrationServiceGrpcFactory {
    fun newInstance(port: Int = 2000, address: String = "localhost"): RegistrationServiceGrpc.RegistrationServiceFutureStub {
        val channel = ManagedChannelBuilder.forAddress(address, port)
            .usePlaintext()
            .build()
        return RegistrationServiceGrpc.newFutureStub(channel)
    }
}