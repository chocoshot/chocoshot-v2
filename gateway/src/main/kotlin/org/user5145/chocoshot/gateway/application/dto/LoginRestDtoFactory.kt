package org.user5145.chocoshot.gateway.application.dto

import org.user5145.chocoshot.shared.rpc.AccountDTO

object LoginRestDtoFactory {
    fun create(dto: AccountDTO) = LoginRestDto(dto.login)
}