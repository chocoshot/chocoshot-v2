package org.user5145.chocoshot.gateway.application

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.core.util.DefaultIndenter
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter
import com.fasterxml.jackson.databind.SerializationFeature
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.jackson.*
import io.ktor.locations.*
import io.ktor.routing.*
import org.slf4j.event.Level

fun Application.installRequiredFeatures() {
    install(CallLogging){ level = Level.INFO }
    install(Locations)
    installJson()
    install(DefaultHeaders)
    install(Compression)
    install(Routing)
    setUpCors()
}

fun Application.setUpCors(){
    install(CORS){
        method(HttpMethod.Post)
        method(HttpMethod.Get)
        header(HttpHeaders.ContentType)
        allowSameOrigin = true
        allowCredentials = true
        hosts.add("http://localhost:8080")
        hosts.add("http://localhost:3000")
        //anyHost()
    }
}

fun Application.installJson() {
    install(ContentNegotiation) {
        jackson {
            configure(SerializationFeature.INDENT_OUTPUT, true)
            setSerializationInclusion(JsonInclude.Include.NON_NULL)
            setDefaultPrettyPrinter(DefaultPrettyPrinter().apply {
                indentArraysWith(DefaultPrettyPrinter.FixedSpaceIndenter.instance)
                indentObjectsWith(DefaultIndenter("  ", "\n"))
            })
        }
    }
}
