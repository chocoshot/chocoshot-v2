package org.user5145.chocoshot.gateway.application.versioning

import io.grpc.stub.StreamObserver

object GrpcProxyService {
    fun <T: Any> proxyResult(responseObserver: StreamObserver<T>, function: () -> T){
        kotlin.runCatching {
            responseObserver.onNext(function.invoke())
        }.onSuccess {
            responseObserver.onCompleted()
        }.onFailure {
            responseObserver.onError(it)
        }
    }
}