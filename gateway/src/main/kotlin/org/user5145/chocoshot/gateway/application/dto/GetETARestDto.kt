package org.user5145.chocoshot.gateway.application.dto

data class GetETARestDto(val timestamp: Long)