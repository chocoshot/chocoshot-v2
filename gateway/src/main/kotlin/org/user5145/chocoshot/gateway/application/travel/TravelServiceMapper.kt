package org.user5145.chocoshot.gateway.application.travel

import org.user5145.chocoshot.gateway.application.dto.*
import org.user5145.chocoshot.grpc.ServerPort
import org.user5145.chocoshot.shared.rpc.Coordinates
import org.user5145.chocoshot.shared.rpc.GameMap
import org.user5145.chocoshot.shared.rpc.NewAction
import org.user5145.chocoshot.shared.rpc.TravelServiceGrpc
import shared.Shared.ChampionName
import shared.Shared.Empty

class TravelServiceMapper(
    val serviceGrpc: TravelServiceGrpc.TravelServiceFutureStub = TravelServiceGrpcFactory.newInstance(ServerPort.TRAVEL_SERVICE.value),
    val playerLocationFactory: GetPlayerLocationRestDtoFactory = GetPlayerLocationRestDtoFactory,
    val etaFactory: GetETARestDtoFactory = GetETARestDtoFactory,
    val queueFactory: GetQueueRestDtoFactory = GetQueueRestDtoFactory,
) {
    fun addMap(name: String, json: String): Empty {
        val outgoingDto = GameMap.newBuilder()
            .setName(name)
            .setJson(json)
            .build()
        return serviceGrpc.addMap(outgoingDto).get()
    }

    fun getPlayerLocation(name: String): GetPlayerLocationRestDto {
        val outgoingDto = ChampionName.newBuilder()
            .setName(name)
            .build()
        val response = serviceGrpc.getPlayerLocation(outgoingDto).get()
        return playerLocationFactory.create(response)
    }

    fun goto(name: String, x: Int, y: Int): GetETARestDto {
        val outgoingDto = NewAction.newBuilder()
            .setName(ChampionName.newBuilder().setName(name))
            .setCoords(Coordinates.newBuilder().setX(x).setY(y))
            .build()
        val response = serviceGrpc.addToQueue(outgoingDto).get()
        return etaFactory.create(response)
    }

    fun getETA(name: String): GetETARestDto {
        val outgoingDto = ChampionName.newBuilder()
            .setName(name)
            .build()
        val response = serviceGrpc.getPlayerETA(outgoingDto).get()
        return etaFactory.create(response)
    }

    fun getQueue(name: String): GetQueueRestDto {
        val outgoingDto = ChampionName.newBuilder()
            .setName(name)
            .build()
        val response = serviceGrpc.getQueue(outgoingDto).get()
        return queueFactory.create(response)
    }

    fun removeLastFromQueue(name: String): Empty {
        val outgoingDto = ChampionName.newBuilder()
            .setName(name)
            .build()
        val response = serviceGrpc.removeLastFromQueue(outgoingDto).get()
        return response
    }

    fun teleportAllChampions(): Empty{
        val outgoingDto = Empty.newBuilder()
            .build()
        val response = serviceGrpc.teleportAllChampions(outgoingDto).get()
        return response
    }
}