package org.user5145.chocoshot.gateway.application.fight

import org.user5145.chocoshot.grpc.ServerPort
import org.user5145.chocoshot.shared.rpc.*
import shared.Shared.ChampionName
import shared.Shared.Empty

class FightServiceMapper(
    val serviceGrpc: FightServiceGrpc.FightServiceFutureStub = FightServiceGrpcFactory.newInstance(ServerPort.FIGHT_SERVICE.value),
    val enemiesRestFactory: EnemiesRestFactory = EnemiesRestFactory,
    val fightRestFactory: FightsRestFactory = FightsRestFactory,
) {

    fun setStats(name: String, hp: Float, strength: Float): Empty {
        val nameBuilder = ChampionName.newBuilder().setName(name)

        val outgoingDto = StatsRatio.newBuilder()
            .setName(nameBuilder)
            .setHp(hp)
            .setStrength(strength)
            .build()
        val response = serviceGrpc.setStats(outgoingDto).get()
        return response
    }

    fun getHistory(name: String): List<FightsRestDto> {
        val championName = ChampionName.newBuilder().setName(name).build()
        val responseGrpc = serviceGrpc.getHistory(championName).get()
        val response = fightRestFactory.create(responseGrpc)
        return response
    }

    fun attack(name: String): Empty {
        val championName = ChampionName.newBuilder().setName(name).build()
        val response = serviceGrpc.attack(championName).get()
        return response
    }

    fun addEnemy(name: String, hpMax: Int, hpMin: Int, levelMin: Int, levelMax: Int, strengthMin: Int, strengthMax: Int): Empty {
        val enemyName = EnemyName.newBuilder().setValue(name)
        val enemy = Enemy.newBuilder()
            .setName(enemyName)
            .setHpMax(hpMax)
            .setHpMin(hpMin)
            .setLevelMax(levelMax)
            .setLevelMin(levelMin)
            .setStrengthMax(strengthMax)
            .setStrengthMin(strengthMin)
            .build()
        val response = serviceGrpc.addEnemy(enemy).get()
        return response
    }

    fun getEnemies(): List<EnemiesRestDto> {
        val empty = Empty.getDefaultInstance()
        val responseGrpc = serviceGrpc.getEnemies(empty).get()
        val response = enemiesRestFactory.create(responseGrpc)
        return response
    }

    fun removeEnemy(enemyName: String): Empty {
        val empty = EnemyName.newBuilder().setValue(enemyName).build()
        val response = serviceGrpc.removeEnemy(empty).get()
        return response
    }
}

object FightsRestFactory{
    fun create(fights: Fights): List<FightsRestDto> {
        return fights.fightsList.map{
            FightsRestDto(it.id, it.level, it.experience, it.success, it.timestamp, it.historyList.map{ turn -> turn.mapToTurnRestDto() })
        }
    }

    private fun FightTurn.mapToTurnRestDto(): TurnRestDto{
        return TurnRestDto(id, attacker, attackerHp, attackerDamageDone, culprit, culpritHp, culpritDamageDone, whoIsPlaying)
    }
}

data class FightsRestDto(
    val id: Int,
    val level: Int,
    val experience: Long,
    val success: Boolean,
    val timestamp: Long,
    val mapIndexed: List<TurnRestDto>
)

data class TurnRestDto(
    val index: Int,
    val attacker: String,
    val attackerHp: Int,
    val attackerDamageDone: Int,
    val culprit: String,
    val culpritHp: Int,
    val culpritDamageDone: Int,
    val whoIsPlaying: String
)

object EnemiesRestFactory{
    fun create(enemies: Enemies): List<EnemiesRestDto> {
        return enemies.enemiesList.map{
            EnemiesRestDto(it.name.value, it.hpMax, it.hpMin, it.strengthMin, it.strengthMax, it.levelMin, it.levelMax)
        }
    }
}

data class EnemiesRestDto(
    val name: String,
    val hpMax: Int,
    val hpMin: Int,
    val strengthMin: Int,
    val strengthMax: Int,
    val levelMin: Int,
    val levelMax: Int
)