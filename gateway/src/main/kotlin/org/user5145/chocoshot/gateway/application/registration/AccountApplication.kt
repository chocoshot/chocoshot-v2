package org.user5145.chocoshot.gateway.application.registration

import io.ktor.application.*
import io.ktor.application.Application
import io.ktor.auth.*
import io.ktor.http.*
import io.ktor.http.ContentType.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.sessions.*
import mu.KotlinLogging
import org.user5145.chocoshot.gateway.application.bcrypt.BCryptService
import org.user5145.chocoshot.gateway.application.session.Session
import org.user5145.chocoshot.json.extension.toJson

fun Application.accountController(
    registrationServiceMapper: RegistrationServiceMapper = RegistrationServiceMapper(),
    bcrypt: BCryptService = BCryptService() ){
    val logger = KotlinLogging.logger {}
    routing {
        route("/v1/account") {
            post {
                try {
                    val request = call.receiveParameters()
                    val bcryptPass = bcrypt.hash( request["password"]!! )
                    val login = request["login"]!!
                    registrationServiceMapper.register( login, bcryptPass )
                    call.respondText("ok", Text.Plain, HttpStatusCode.Created )
                } catch (e: Exception) {
                    logger.debug { e.stackTraceToString() }
                    call.respondText("use different username or try later", Text.Plain, HttpStatusCode.Conflict )
                }
            }

            get("/{login}/champions") {
                try {
                    val login = call.parameters["login"]!!
                    val response = registrationServiceMapper.champions(login)
                    call.respondText(response.toJson())
                } catch (e: Exception) {
                    logger.debug { e.stackTraceToString() }
                    call.respondText("failed", Text.Plain, HttpStatusCode.NotFound)
                }
            }

            authenticate("account") {
                post("/me/character") {
                    try {
                        val login = call.sessions.get<Session>()!!.account
                        val name = call.receiveParameters()["name"]!!
                        registrationServiceMapper.createChampion(login, name)
                        call.respondText("ok")
                    } catch (e: Exception) {
                        logger.debug { e.stackTraceToString() }
                        call.respondText("failed", Text.Plain, HttpStatusCode.InternalServerError)
                    }
                }
            }
        }
    }
}
