package org.user5145.chocoshot.gateway.application.versioning

import io.grpc.stub.StreamObserver
import org.user5145.chocoshot.gateway.application.travel.TravelServiceGrpcFactory
import org.user5145.chocoshot.grpc.ServerPort
import org.user5145.chocoshot.shared.rpc.*
import shared.Shared.ChampionName
import shared.Shared.Empty

class TravelService(
    private val travelService: TravelServiceGrpc.TravelServiceFutureStub = TravelServiceGrpcFactory.newInstance(ServerPort.TRAVEL_SERVICE.value),
    private val proxy: GrpcProxyService = GrpcProxyService
): TravelServiceGrpc.TravelServiceImplBase()  {
    override fun addToQueue(request: NewAction, responseObserver: StreamObserver<ETA>) {
        proxy.proxyResult(responseObserver) { travelService.addToQueue(request).get() }
    }

    override fun getPlayerLocation(request: ChampionName, responseObserver: StreamObserver<Coordinates>) {
        proxy.proxyResult(responseObserver) { travelService.getPlayerLocation(request).get() }
    }

    override fun getPlayerETA(request: ChampionName, responseObserver: StreamObserver<ETA>) {
        proxy.proxyResult(responseObserver) { travelService.getPlayerETA(request).get() }
    }

    override fun addMap(request: GameMap, responseObserver: StreamObserver<Empty>) {
        proxy.proxyResult(responseObserver) { travelService.addMap(request).get() }
    }

    override fun getQueue(request: ChampionName, responseObserver: StreamObserver<ActionQueue>) {
        proxy.proxyResult(responseObserver) { travelService.getQueue(request).get() }
    }

    override fun removeLastFromQueue(request: ChampionName, responseObserver: StreamObserver<Empty>) {
        proxy.proxyResult(responseObserver) { travelService.removeLastFromQueue(request).get() }
    }
}