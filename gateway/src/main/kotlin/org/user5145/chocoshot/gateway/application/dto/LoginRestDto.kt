package org.user5145.chocoshot.gateway.application.dto

data class LoginRestDto(val login: String)