package org.user5145.chocoshot.gateway.application.dto

import org.user5145.chocoshot.shared.rpc.ETA

object GetETARestDtoFactory {
    fun create(eta: ETA) = GetETARestDto(eta.timestamp)
}