package org.user5145.chocoshot.gateway

import io.ktor.server.engine.*
import io.ktor.server.netty.*
import org.user5145.chocoshot.fight.FightMainService
import org.user5145.chocoshot.gateway.application.versioning.RegistrationMain
import org.user5145.chocoshot.gateway.application.versioning.TravelMain
import org.user5145.chocoshot.registration.Main as RegistrationMainService
import org.user5145.chocoshot.travel.Main as TravelMainService

fun main(args: Array<String> = arrayOf()) {
    RegistrationMainService().main()
    FightMainService().main()
    TravelMainService().main()
    TravelMain().main()
    RegistrationMain().main()
    embeddedServer(Netty, commandLineEnvironment(args)).start()
}