package org.user5145.chocoshot.gateway.application.travel

import io.ktor.application.*
import io.ktor.application.Application
import io.ktor.auth.*
import io.ktor.http.*
import io.ktor.http.ContentType.*
import io.ktor.http.content.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.sessions.*
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.coroutines.yield
import mu.KotlinLogging
import org.user5145.chocoshot.gateway.application.session.Session
import org.user5145.chocoshot.json.extension.toJson
import org.user5145.chocoshot.properties.ConfigContext
import java.io.File
import java.io.InputStream
import java.io.OutputStream
import java.nio.file.Files
import java.nio.file.Path

fun Application.travelController(travelServiceMapper: TravelServiceMapper = TravelServiceMapper(),
                                 config: ConfigContext = ConfigContext) {
    val logger = KotlinLogging.logger {}

    routing {
        route("/v1/travel") {
            authenticate("character") {
                get ("/eta") {
                    try {
                        val name = call.sessions.get<Session>()!!.character.get()
                        val response = travelServiceMapper.getETA(name).toJson()
                        call.respondText(response, Text.Plain, HttpStatusCode.OK)
                    } catch (e: Exception) {
                        call.respondText("Failed", Text.Plain, HttpStatusCode.Conflict)
                    }
                }

                post ("/go-to") {
                    try {
                        val parameters = call.receiveParameters()
                        val name = call.sessions.get<Session>()!!.character.get()
                        val x = parameters["x"]!!.toInt()
                        val y = parameters["y"]!!.toInt()
                        val response = travelServiceMapper.goto(name, x, y).toJson()
                        call.respondText(response, Text.Plain, HttpStatusCode.OK)
                    } catch (e: Exception) {
                        call.respondText("Failed", Text.Plain, HttpStatusCode.Conflict)
                    }
                }

                get ("/queue") {
                    try {
                        val name = call.sessions.get<Session>()!!.character.get()
                        val response = travelServiceMapper.getQueue(name).toJson()
                        call.respondText(response, Text.Plain, HttpStatusCode.OK)
                    } catch (e: Exception) {
                        call.respondText("Failed", Text.Plain, HttpStatusCode.Conflict)
                    }
                }

                delete ("/queue/last") {
                    try {
                        val name = call.sessions.get<Session>()!!.character.get()
                        val response = travelServiceMapper.removeLastFromQueue(name).toJson()
                        call.respondText(response, Text.Plain, HttpStatusCode.OK)
                    } catch (e: Exception) {
                        call.respondText("Failed", Text.Plain, HttpStatusCode.Conflict)
                    }
                }
            }

            get ("/player-location") {
                try {
                    val name = call.receiveParameters()["name"]!!
                    val response = travelServiceMapper.getPlayerLocation(name).toJson()
                    call.respondText(response, Text.Plain, HttpStatusCode.OK)
                } catch (e: Exception) {
                    call.respondText("Failed", Text.Plain, HttpStatusCode.Conflict)
                }
            }

            authenticate ( "admin" ) {
                post("/teleport/all") {
                    try {
                        travelServiceMapper.teleportAllChampions()
                        call.respondText("ok", Text.Plain, HttpStatusCode.OK)
                    } catch (e: Exception) {
                        call.respondText("Failed", Text.Plain, HttpStatusCode.Conflict)
                    }
                }

                post("/map") {
                    try {
                        log.warn("Loading new map. It's recommended to teleport all the users to a safe location")
                        var name: String? = null
                        var path: String? = null
                        val multi = call.receiveMultipart().readAllParts()
                        multi.forEach {
                            when (it) {
                                is PartData.FormItem -> {
                                    if (it.name == "name") {
                                        name = it.value
                                    }
                                }
                                is PartData.FileItem -> {
                                    println("file")
                                    val ext = File(it.originalFileName!!).extension
                                    val file = File(File("D:/tmp/"), "upload-${System.currentTimeMillis()}.$ext")
                                    it.streamProvider().use { input ->
                                        file.outputStream().buffered().use { output -> input.copyToSuspend(output) }
                                    }
                                    path = file.absolutePath
                                }
                            }
                            it.dispose
                        }

                        val json = Files.readString(Path.of(path!!))
                        travelServiceMapper.addMap(name!!, json)
                        call.respondText("ok", Text.Plain, HttpStatusCode.Created)
                    } catch (e: Exception) {
                        call.respondText("", Text.Plain, HttpStatusCode.NotFound)
                    }
                }
            }
        }
    }
}

//this super easy code is from the docs https://ktor.io/docs/multipart-support.html#receiving-files-using-multipart
suspend fun InputStream.copyToSuspend(
    out: OutputStream,
    bufferSize: Int = DEFAULT_BUFFER_SIZE,
    yieldSize: Int = 4 * 1024 * 1024,
    dispatcher: CoroutineDispatcher = Dispatchers.IO
): Long {
    return withContext(dispatcher) {
        val buffer = ByteArray(bufferSize)
        var bytesCopied = 0L
        var bytesAfterYield = 0L
        while (true) {
            val bytes = read(buffer).takeIf { it >= 0 } ?: break
            out.write(buffer, 0, bytes)
            if (bytesAfterYield >= yieldSize) {
                yield()
                bytesAfterYield %= yieldSize
            }
            bytesCopied += bytes
            bytesAfterYield += bytes
        }
        return@withContext bytesCopied
    }
}
