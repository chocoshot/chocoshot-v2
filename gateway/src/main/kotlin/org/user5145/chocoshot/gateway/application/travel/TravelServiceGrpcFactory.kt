package org.user5145.chocoshot.gateway.application.travel

import io.grpc.ManagedChannelBuilder
import org.user5145.chocoshot.shared.rpc.TravelServiceGrpc

object TravelServiceGrpcFactory {
    fun newInstance(port: Int = 2001, address: String = "localhost"): TravelServiceGrpc.TravelServiceFutureStub {
        val channel = ManagedChannelBuilder.forAddress(address, port)
            .usePlaintext()
            .build()
        return TravelServiceGrpc.newFutureStub(channel)
    }
}