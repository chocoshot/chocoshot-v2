package org.user5145.chocoshot.shared.champion

import io.grpc.stub.StreamObserver
import org.user5145.chocoshot.shared.rpc.*

class TestServiceImpl(val responseValue: String): TestServiceGrpc.TestServiceImplBase() {
    override fun sayHi(request: Hello, responseObserver: StreamObserver<Hello>) {
        val response = Hello.newBuilder().setMessage(responseValue).build()
        responseObserver.onNext(response)
        println(request.message)
        responseObserver.onCompleted()
    }
}