package org.user5145.chocoshot.shared.champion

import io.grpc.ManagedChannelBuilder
import io.grpc.ServerBuilder
import mu.KotlinLogging
import org.junit.Assert
import org.user5145.chocoshot.shared.rpc.Hello
import org.user5145.chocoshot.shared.rpc.TestServiceGrpc
import kotlin.test.Test

class UsageServiceTest {
    private val log = KotlinLogging.logger {}
    private val responseValue = "Hi"

    @Test
    fun example(){
        val server = ServerBuilder
            .forPort(3000)
            .addService(TestServiceImpl(responseValue))
            .build()
        log.info { "starting server" }
        server.start()
        log.info { "started server" }

        val channel = ManagedChannelBuilder.forAddress("localhost", 3000)
            .usePlaintext()
            .build()

        val stub: TestServiceGrpc.TestServiceFutureStub
        = TestServiceGrpc.newFutureStub(channel)

        val response = stub.sayHi(Hello
            .newBuilder()
            .setMessage("Test Message")
            .build())

        Assert.assertEquals(responseValue, response.get().message)

        channel.shutdown()
        server.shutdownNow()
    }
}