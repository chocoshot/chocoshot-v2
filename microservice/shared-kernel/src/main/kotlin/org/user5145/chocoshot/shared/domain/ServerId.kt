package org.user5145.chocoshot.shared.domain

data class ServerId(val value: Int)