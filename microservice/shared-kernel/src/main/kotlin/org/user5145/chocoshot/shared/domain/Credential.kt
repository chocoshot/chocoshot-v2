package org.user5145.chocoshot.shared.domain

data class Credential(val loginId: LoginId, val password: String)
