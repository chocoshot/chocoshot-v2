package org.user5145.chocoshot.shared.domain

enum class MapBlockType {
    BLOCKING,
    WORLD_CENTER,
    SPAWN,
    ENEMY
}