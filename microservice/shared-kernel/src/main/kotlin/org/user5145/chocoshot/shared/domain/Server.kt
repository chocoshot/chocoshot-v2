package org.user5145.chocoshot.shared.domain

data class Server (val id: ServerId, var name: ServerName)