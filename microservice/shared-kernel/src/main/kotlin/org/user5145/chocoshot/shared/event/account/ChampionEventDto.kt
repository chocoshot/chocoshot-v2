package org.user5145.chocoshot.shared.event.account

data class ChampionEventDto(val name: String)