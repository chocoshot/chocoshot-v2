package org.user5145.chocoshot.shared.event.account

import org.bson.types.ObjectId
import org.litote.kmongo.Id

data class AddChampionEventDto(val id: ObjectId, val name: String)