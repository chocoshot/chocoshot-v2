package org.user5145.chocoshot.shared.event.account

enum class RegistrationTopicIds {
    CREATED_CHAMPION,
    CREATED_ACCOUNT,
    STEPPED_ON_BLOCK
}
