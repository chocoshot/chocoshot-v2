package org.user5145.chocoshot.shared.event.account

import org.bson.types.ObjectId

data class AddAccountEventDto(
    val id: ObjectId,
    val login: String,
    val password: String,
    val champions: List<ChampionEventDto>)