package org.user5145.chocoshot.shared.event.account

import org.user5145.chocoshot.shared.domain.MapBlockType

data class SteppedOnBlockEventDto(val name: String, val effects: List<MapBlockType>)