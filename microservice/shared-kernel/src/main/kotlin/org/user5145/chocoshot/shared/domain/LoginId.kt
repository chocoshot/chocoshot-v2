package org.user5145.chocoshot.shared.domain

import java.io.Serializable

class LoginId(value: String) : Serializable{
    val value = value.toLowerCase()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as LoginId

        if (value != other.value) return false

        return true
    }

    override fun hashCode(): Int {
        return value.hashCode()
    }
}
