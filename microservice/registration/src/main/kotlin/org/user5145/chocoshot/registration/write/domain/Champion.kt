package org.user5145.chocoshot.registration.write.domain

data class Champion(
    val name: String
)
