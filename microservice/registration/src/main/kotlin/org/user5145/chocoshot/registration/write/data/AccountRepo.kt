package org.user5145.chocoshot.registration.write.data

import com.mongodb.client.MongoDatabase
import com.mongodb.client.model.Filters.*
import com.mongodb.client.model.IndexOptions
import com.mongodb.client.model.Indexes
import com.mongodb.client.model.UpdateOptions
import org.litote.kmongo.*
import org.user5145.chocoshot.db.mongo.MongoDataSource
import org.user5145.chocoshot.registration.write.domain.Account
import org.user5145.chocoshot.registration.write.domain.Champion

class AccountRepo() {
    init {
        val collection = getSchema().getCollection<Account>()
        val indexOptions: IndexOptions = IndexOptions().unique(true)
        collection.createIndex(Indexes.ascending(Account::login.name), indexOptions)
    }

    fun get(login: String): Account {
        val database = getSchema()
        val collection = database.getCollection<Account>()
        return collection.findOne(eq(Account::login.name, login)) ?: throw Exception()
    }

    fun getByChampionOrNull(givenName: String): Account? {
        val database = getSchema()
        val collection = database.getCollection<Account>()
        return collection.findOne("""{ "champions": { name: "$givenName" } }""" )
    }

    fun get(id: Id<Account>): Account {
        val database = getSchema()
        val collection = database.getCollection<Account>()
        return collection.findOneById(id) ?: throw Exception()
    }

    fun upsert(account: Account) {
        val database = getSchema()
        val collection = database.getCollection<Account>()
        val options = UpdateOptions().upsert(true)
        collection.updateOneById(account._id, account, options)
    }

    private fun getSchema(): MongoDatabase =
        MongoDataSource.mongoClient.getDatabase("account")
}
