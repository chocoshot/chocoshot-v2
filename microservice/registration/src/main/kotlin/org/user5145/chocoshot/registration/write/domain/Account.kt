package org.user5145.chocoshot.registration.write.domain

import org.bson.types.ObjectId
import org.user5145.chocoshot.amqp.kafka.PushEvent
import org.user5145.chocoshot.amqp.kafka.ServerId
import org.user5145.chocoshot.json.extension.toJson
import org.user5145.chocoshot.shared.event.account.AddAccountEventDto
import org.user5145.chocoshot.shared.event.account.AddChampionEventDto
import org.user5145.chocoshot.shared.event.account.ChampionEventDto
import org.user5145.chocoshot.shared.event.account.RegistrationTopicIds

data class Account (
    val _id: ObjectId = ObjectId(),
    val login: String,
    val password: String,
    val champions: MutableList<Champion> = ArrayList(),
    val admin: Boolean = false
) {

    fun addChampion(name: String) {
        if (champions.stream().anyMatch { it.name == name })
            throw Exception()

        val newChampion = Champion(name)
        champions.add(newChampion)
        sendAddChampionEvent(newChampion)
    }

    fun sendAddAccountEvent() {
        val championsDto = champions.map { ChampionEventDto(it.name) }.toList()
        val dto = AddAccountEventDto(id = _id,  login = login, password = password, champions = championsDto )
        PushEvent.push(topic = RegistrationTopicIds.CREATED_ACCOUNT.name,
            key = "1.0",
            data = dto.toJson(),
            id = ServerId.REGISTRATION.name)
    }

    private fun sendAddChampionEvent(newChampion: Champion) {
        val dto = AddChampionEventDto(id = _id, name = newChampion.name)
        PushEvent.push(topic = RegistrationTopicIds.CREATED_CHAMPION.name,
            key = "1.0",
            data = dto.toJson(),
            id = ServerId.REGISTRATION.name)
    }
}