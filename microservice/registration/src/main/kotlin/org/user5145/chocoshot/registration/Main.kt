package org.user5145.chocoshot.registration

import org.user5145.chocoshot.grpc.GrpcServer
import org.user5145.chocoshot.grpc.ServerPort
import org.user5145.chocoshot.registration.application.RegistrationService
import org.user5145.chocoshot.registration.write.event.EventHandler

class Main(){
    fun main(){
        EventHandler.start()

        GrpcServer.start(RegistrationService(), ServerPort.REGISTRATION_SERVICE)
    }

    fun shutdown() {
        GrpcServer.shutdown()
    }
}
