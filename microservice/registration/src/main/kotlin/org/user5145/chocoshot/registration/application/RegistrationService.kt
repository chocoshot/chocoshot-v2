package org.user5145.chocoshot.registration.application

import io.grpc.Status
import io.grpc.stub.StreamObserver
import mu.KotlinLogging
import org.user5145.chocoshot.registration.write.data.AccountRepo
import org.user5145.chocoshot.registration.write.domain.Account
import org.user5145.chocoshot.shared.rpc.*
import shared.Shared.Empty

class RegistrationService(val repo: AccountRepo = AccountRepo()) : RegistrationServiceGrpc.RegistrationServiceImplBase() {
    val logger = KotlinLogging.logger {}

    override fun register(request: RegisterAccountDTO, responseObserver: StreamObserver<Empty>) {
        runCatching {
            val account = Account(login = request.login, password = request.pass)
            repo.upsert(account)
            account.sendAddAccountEvent()
        }.onSuccess {
            responseObserver.onNext(Empty.getDefaultInstance())
            responseObserver.onCompleted()
        }.onFailure {
            logger.debug { it.stackTraceToString() }
            responseObserver.onError(it)
        }
    }

    override fun createChampion(request: CreateChampionDTO, responseObserver: StreamObserver<Empty>) {
        runCatching {
            val championWithGivenName = repo.getByChampionOrNull(request.name)
            val account = repo.get(request.login)
            if (championWithGivenName != null)
                throw Status.ALREADY_EXISTS.asException()
            account.addChampion(request.name)
            repo.upsert(account)
        }.onSuccess {
            responseObserver.onNext(Empty.getDefaultInstance())
            responseObserver.onCompleted()
        }.onFailure {
            logger.debug { it.stackTraceToString() }
            responseObserver.onError(it)
        }
    }

    override fun getAccountsChampions(request: GetAccountsChampionsDTO, responseObserver: StreamObserver<ChampionsDTO>) {
        runCatching {
            val account = repo.get(request.login)
            val champions: Iterable<ChampionDTO> = account.champions.map { ChampionDTO.newBuilder().setName(it.name).build() }
            val dto = ChampionsDTO.newBuilder().addAllChampions(champions).build()
            responseObserver.onNext(dto)
        }.onSuccess {
            responseObserver.onCompleted()
        }.onFailure {
            logger.debug { it.stackTraceToString() }
            responseObserver.onError(Status.NOT_FOUND.asException())
        }
    }

    override fun getAccount(request: AccountRequest, responseObserver: StreamObserver<AccountDTO>) {
        runCatching {
            val account = repo.get(request.login)
            responseObserver.onNext(AccountDTO.newBuilder().setLogin(account.login).setPassword(account.password).setIsAdmin(account.admin).build())
            responseObserver.onCompleted()
        }.onFailure {
            logger.info { it.stackTraceToString() }
            responseObserver.onError(Status.UNAVAILABLE.asException())
        }
    }
}
