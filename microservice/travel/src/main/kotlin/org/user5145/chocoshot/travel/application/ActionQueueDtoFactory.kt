package org.user5145.chocoshot.travel.application

import org.user5145.chocoshot.shared.rpc.ActionQueue
import org.user5145.chocoshot.shared.rpc.Coordinates
import org.user5145.chocoshot.travel.write.domain.player.Action
import org.user5145.chocoshot.shared.rpc.Action as GrpcAction

object ActionQueueDtoFactory {
    fun create(queueLimit: Int, actions: List<Action>): ActionQueue {
        val builder = ActionQueue.newBuilder()
        val grpcActions = actions.mapIndexed { index, action -> action.mapActionsToGrpcActions(index) }
        builder.addAllActions(grpcActions)
        builder.queueLimit = queueLimit
        return builder.build()
    }

    private fun Action.mapActionsToGrpcActions(index: Int): GrpcAction {
        val coords = Coordinates.newBuilder().setX(coordinates.x).setY(coordinates.y).build()
        return GrpcAction.newBuilder().setId(index).setTimestamp(eTA).setCoords(coords).build()
    }
}