package org.user5145.chocoshot.travel.write.domain.algorithm

data class Node(val x: Int,
                val y: Int,
                var blocked: Boolean = false,
                var parent: Node? = null,
                var gScore: Int?,
                var fScore: Int?)
