package org.user5145.chocoshot.travel.write.event

internal enum class TravelTopicIds {
    MAP_ADDED,
    PLAYER_MOVED
}
