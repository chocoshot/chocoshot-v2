package org.user5145.chocoshot.travel.application

import org.user5145.chocoshot.shared.rpc.Coordinates
import org.user5145.chocoshot.travel.write.domain.map.json.Coordinates as IncomingCoordinates

object CoordinatesDtoFactory {
    fun create(coordinates: IncomingCoordinates): Coordinates {
        return Coordinates.newBuilder().setX(coordinates.x).setY(coordinates.y).build()
    }
}