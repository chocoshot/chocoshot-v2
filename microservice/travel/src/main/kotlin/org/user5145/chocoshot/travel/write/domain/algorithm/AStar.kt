package org.user5145.chocoshot.travel.write.domain.algorithm

import mu.KotlinLogging

object AStar {
    val logger = KotlinLogging.logger {}

    fun getPath(start: Coords, end: Coords, map: Array<Coords>): List<Coords>{
        try {
            val openSet = hashMapOf<Coords, Node>()
            val closed = hashMapOf<Coords, Node>()
            openSet[start] = Node(
                x = start.x,
                y = start.y,
                blocked = false,
                fScore = getFScore(start, end, map),
                gScore = 0
            )

            while(true) {
                var current = openSet.values.minByOrNull { it.fScore!! }!!
                openSet.remove(Coords(current.x, current.y))
                closed[Coords(current.x, current.y)] = current
                if ( end.x == current.x && end.y == current.y ){
                    closed[Coords(current.x, current.y)] = current
                    return recreatePath(end, start, closed)
                }

                val nodes = getNeighbours(Coords(current.x, current.y), map)
                nodes.forEach {
                    if ( !(it.blocked || closed.contains(it) )
                        && (!openSet.contains(it) || getGScore(current, it) < openSet[it]!!.gScore!!)){
                        val node = Node(
                            x = it.x,
                            y = it.y,
                            blocked = it.blocked,
                            fScore = getFScore(it, Coords(end.x, end.y), map),
                            gScore = getGScore(current, it),
                            parent = current
                        )

                        if (!openSet.containsKey(Coords(it.x, it.y)))
                            openSet[Coords(it.x, it.y)] = node
                    }
                }
            }
        } catch (ex: Exception) {
            throw NoSuchPath()
        }

    }

    private fun getFScore(start: Coords, end: Coords, map: Array<Coords>): Int {
        if (start.x == end.x && start.y != end.y){
            val newStart = if (start.y < end.y)
                Coords(start.x, start.y +1)
            else
                Coords(start.x, start.y -1)
            return getFScore(newStart, end, map) + 10

        } else if (start.x != end.x && start.y == end.y){
            val newStart = if (start.x < end.x)
                Coords(start.x +1, start.y)
            else
                Coords(start.x -1, start.y)
            return getFScore(newStart, end, map) + 10

        } else if (start.x != end.x && start.y != end.y){
            var newStart: Coords = if (start.x < end.x && start.y < end.y) {
                Coords(start.x +1, start.y +1)
            } else if (start.x > end.x && start.y < end.y) {
                Coords(start.x -1, start.y +1)
            } else if (start.x > end.x && start.y > end.y) {
                Coords(start.x -1, start.y -1)
            } else {
                Coords(start.x +1, start.y  -1)
            }
            return getFScore(newStart, end, map) + 14

        } else {
            return 0
        }
    }

    private fun getNeighbours(current: Coords, map: Array<Coords>) = map.filter { current.x-1 <= it.x && it.x <= current.x+1 && current.y-1 <= it.y && it.y <= current.y+1 }

    private fun getGScore(current: Node, target: Coords): Int{
        if (current.x != target.x && current.y != target.y) {
            return current.gScore!! + 14
        }
        return current.gScore!! + 10
    }

    private fun recreatePath(end: Coords, start: Coords, closed: HashMap<Coords, Node>): List<Coords> {
        var current = end
        var result = arrayListOf(current)
        while(start != current){
            current = getNeighbours(current, closed.keys.toTypedArray()).minByOrNull { closed[it]!!.gScore!! }!!
            result.add(current)
        }
        logger.debug { "AStar suggested path is: ${result}" }
        return result
    }
}