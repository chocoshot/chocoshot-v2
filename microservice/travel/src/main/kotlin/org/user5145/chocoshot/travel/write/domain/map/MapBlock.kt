package org.user5145.chocoshot.travel.write.domain.map

import org.user5145.chocoshot.shared.domain.MapBlockType
import org.user5145.chocoshot.travel.write.domain.map.json.Coordinates

data class MapBlock(
    val effects: MutableList<MapBlockType> = ArrayList(),
    val coordinates: Coordinates,
) {
    fun addEffect(effect: MapBlockType){
        if (!effects.contains(effect))
            effects.add(effect)
    }

    fun blocked() = effects.contains(MapBlockType.BLOCKING)
}