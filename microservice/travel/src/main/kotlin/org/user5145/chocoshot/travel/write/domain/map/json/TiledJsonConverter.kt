package org.user5145.chocoshot.travel.write.domain.map.json

import mu.KLogger
import mu.KotlinLogging
import org.user5145.chocoshot.json.extension.jsonToObject
import org.user5145.chocoshot.shared.domain.MapBlockType
import org.user5145.chocoshot.travel.write.domain.map.MapBlock
import org.user5145.chocoshot.travel.write.domain.map.MapBlockFactory
import org.user5145.chocoshot.travel.write.domain.map.SpecialLayersNames

object TiledJsonConverter {
    val logger = KotlinLogging.logger {}
    val mapBlockFactory = MapBlockFactory

    fun convert(json: String): MutableList<MapBlock>  {
        logger.debug { "received Json: ${json}" }
        val map: TiledMapRootDto = json.jsonToObject()
        return convertTiledObjectToMapBlocks(map)
    }

    private fun convertTiledObjectToMapBlocks(map: TiledMapRootDto): MutableList<MapBlock> {
        val center = getCenterCoordinates(map)
        val blocks = initializeBlocks(map, center)
        setEffects(map.layers, blocks)
        return blocks
    }

    private fun initializeBlocks(map: TiledMapRootDto, center: Coordinates): MutableList<MapBlock> {
        val blocks: MutableList<MapBlock> = ArrayList()
        forEachBlock(map.layers) { actualLayer, actualWidth, actualHeight, logger ->
            lateinit var block: MapBlock
            if (blocks.size < rightDownIndex(actualWidth,actualHeight, actualLayer.width)) {
                block = mapBlockFactory.create(x = actualWidth - center.x, y = actualHeight - center.y)
                blocks.add(block)
            }
        }
        return blocks
    }

    private fun setEffects(layers: MutableList<TiledLayerDto>, blocks: MutableList<MapBlock>): MutableList<Coordinates> {
        val spawns: MutableList<Coordinates> = ArrayList()
        forEachBlock(layers) { actualLayer, actualWidth, actualHeight, logger ->
                var block = blocks[rightDownIndexFromZero(actualWidth, actualHeight, actualLayer.width)]
                if (actualLayer.data[rightDownIndexFromZero(actualWidth, actualHeight, actualLayer.width)] != 0L) {
                    when (actualLayer.name) {
                        SpecialLayersNames.SPAWN.value -> {
                            logger.debug { "Spawn detected at width: ${block.coordinates.x}, height: ${block.coordinates.y}." }
                            block.addEffect(MapBlockType.SPAWN)
                        }
                        SpecialLayersNames.COLLISION.value -> {
                            logger.debug { "Blocking block detected at width: ${block.coordinates.x}, height: ${block.coordinates.y}." }
                            block.addEffect(MapBlockType.BLOCKING)
                        }
                        SpecialLayersNames.ENEMY.value -> {
                            logger.debug { "Enemy block detected at width: ${block.coordinates.x}, height: ${block.coordinates.y}." }
                            block.addEffect(MapBlockType.ENEMY)
                        }
                    }
                }
            }
        return spawns
    }

    private fun getCenterCoordinates(map: TiledMapRootDto): Coordinates {
        var result: Coordinates? = null
        forEachBlock(map.layers) { actualLayer, actualWidth, actualHeight, logger ->
            if (actualLayer.name == SpecialLayersNames.MAP_CENTER.value && actualLayer.data[rightDownIndexFromZero( actualWidth, actualHeight, actualLayer.width )] != 0L) {
                    logger.info("map center found at width: $actualWidth, height: $actualHeight")
                    result = Coordinates(x = actualWidth, y = actualHeight)
                    return@forEachBlock
            }
        }
        return result ?: throw MapCenterNotFoundException()
    }

    private fun forEachBlock(layers: MutableList<TiledLayerDto>, function: (actualLayer: TiledLayerDto, actualWidth: Int, actualHeight: Int, logger: KLogger)-> Unit){
        val logger = KotlinLogging.logger {}
        for ( actualLayer in 0 until layers.size){
            for ( actualHeight in 0 until layers[actualLayer].height ) {
                for ( actualWidth in 0 until layers[actualLayer].width ){
                    function(layers[actualLayer], actualWidth, actualHeight, logger)
                }
            }
        }
    }

    private fun rightDownIndexFromZero(width: Int, height: Int, widthMax: Int): Int = rightDownIndex(width= width, height = height, widthMax = widthMax) -1

    private fun rightDownIndex(width: Int, height: Int, widthMax: Int): Int = height*widthMax + width+1
}