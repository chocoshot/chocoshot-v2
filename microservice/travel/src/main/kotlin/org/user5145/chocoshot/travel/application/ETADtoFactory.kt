package org.user5145.chocoshot.travel.application

import org.user5145.chocoshot.shared.rpc.ETA

object ETADtoFactory {
    fun create(eTA: Long): ETA {
        return ETA.newBuilder().setTimestamp(eTA).build()
    }
}