package org.user5145.chocoshot.travel.write.domain

data class Seconds(val value: Long)