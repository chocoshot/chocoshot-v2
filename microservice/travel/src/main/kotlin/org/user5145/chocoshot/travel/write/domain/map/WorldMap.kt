package org.user5145.chocoshot.travel.write.domain.map

import org.litote.kmongo.Id
import org.litote.kmongo.newId
import org.user5145.chocoshot.shared.domain.MapBlockType
import org.user5145.chocoshot.travel.write.domain.algorithm.AStar
import org.user5145.chocoshot.travel.write.domain.algorithm.Coords
import org.user5145.chocoshot.travel.write.domain.map.json.Coordinates
import org.user5145.chocoshot.travel.write.domain.map.json.TiledJsonConverter
import org.user5145.chocoshot.travel.write.domain.player.Player

data class WorldMap(
    val _id: Id<Player> = newId(),
    val name: String,
    var blocks: MutableList<MapBlock> = ArrayList(),
    private val tiledJsonConverter: TiledJsonConverter = TiledJsonConverter,
    private val aStar: AStar = AStar
    ) {

    fun replace(blocksJson: String) {
        this.blocks = tiledJsonConverter.convert(blocksJson)
    }

    fun provideSpawns() = blocks.filter { it.effects.contains(MapBlockType.SPAWN) }

    fun getPath(start: Coordinates, end: Coordinates): List<Coordinates> {
        val startArg: Coords = Coords(x = start.x, y = start.y, blocked = getBlockByCoords(start).blocked())
        val endArg: Coords = Coords(x = end.x, y = end.y, blocked = getBlockByCoords(end).blocked())
        val path = aStar.getPath(
            start = startArg,
            end = endArg,
            map = blocks.map { Coords(it.coordinates.x, it.coordinates.y, it.blocked()) }.toTypedArray()
        )
        return path.map { Coordinates(it.x, it.y) }
    }

    fun blocksAsArray(): Array<Coords> {
        return blocks.map { Coords(it.coordinates.x, it.coordinates.y, it.blocked()) }.toTypedArray()
    }

    fun getEffects(x: Int, y: Int): MutableList<MapBlockType> {
        return blocks.parallelStream().filter { it.coordinates.x == x && it.coordinates.y == y  }.findAny().get().effects
    }

    fun getBlockWithEffect(effect: MapBlockType): List<Coords> {
        return blocks.filter { it.effects.contains(effect) }.map { Coords(it.coordinates.x, it.coordinates.y, it.blocked()) }
    }

    private fun getBlockByCoords(coordinates: Coordinates) = blocks.first { it.coordinates == coordinates }
}