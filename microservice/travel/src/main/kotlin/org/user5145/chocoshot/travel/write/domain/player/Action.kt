package org.user5145.chocoshot.travel.write.domain.player

import org.user5145.chocoshot.travel.write.domain.map.json.Coordinates

data class Action (
    var coordinates: Coordinates,
    var eTA: Long = 0L
)