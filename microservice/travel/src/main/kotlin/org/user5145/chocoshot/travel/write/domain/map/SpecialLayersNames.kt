package org.user5145.chocoshot.travel.write.domain.map

enum class SpecialLayersNames(val value: String) {
    MAP_CENTER("map center"),
    SPAWN("spawn"),
    COLLISION("collision"),
    ENEMY("enemy")
}