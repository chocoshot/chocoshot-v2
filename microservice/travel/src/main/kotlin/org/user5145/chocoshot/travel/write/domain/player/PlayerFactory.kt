package org.user5145.chocoshot.travel.write.domain.player

import org.bson.types.ObjectId
import org.user5145.chocoshot.travel.write.domain.map.MapBlock
import org.user5145.chocoshot.travel.write.domain.map.json.Coordinates

object PlayerFactory {
    fun newPlayer(name: String, mapName: String, spawns: List<MapBlock>, ): Player {
        val coordinates = Coordinates(spawns.first().coordinates.x, spawns.first().coordinates.y)
        return Player(name = name, mapName = mapName, coordiantes = coordinates)
    }
}