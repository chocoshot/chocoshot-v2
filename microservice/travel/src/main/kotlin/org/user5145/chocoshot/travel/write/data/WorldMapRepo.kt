package org.user5145.chocoshot.travel.write.data

import com.mongodb.client.MongoDatabase
import com.mongodb.client.model.Filters
import com.mongodb.client.model.IndexOptions
import com.mongodb.client.model.Indexes
import com.mongodb.client.model.UpdateOptions
import org.litote.kmongo.*
import org.user5145.chocoshot.db.mongo.MongoDataSource
import org.user5145.chocoshot.travel.write.domain.map.WorldMap

class WorldMapRepo {
    init {
        val collection = getSchema().getCollection<WorldMap>()
        val indexOptions: IndexOptions = IndexOptions().unique(true)
        collection.createIndex(Indexes.ascending(WorldMap::name.name), indexOptions)
    }

    fun get(id: Id<Any>): WorldMap {
        val database = getSchema()
        val collection = database.getCollection<WorldMap>()
        return collection.findOneById(id) ?: throw Exception()
    }

    fun get(name: String): WorldMap {
        val database = getSchema()
        val collection = database.getCollection<WorldMap>()
        val world =  collection.findOne(Filters.eq(WorldMap::name.name, name))
        world ?: throw Exception()
        return world
    }

    fun get(): MutableList<WorldMap> {
        val database = getSchema()
        val collection = database.getCollection<WorldMap>()
        val world =  collection.find().toMutableList()
        return world
    }

    fun upsert(map: WorldMap) {
        val database = getSchema()
        val collection = database.getCollection<WorldMap>()
        val options = UpdateOptions().upsert(true)
        collection.updateOneById(map._id, map, options)
    }

    fun exists(name: String): Boolean {
        try {
            val database = getSchema()
            val collection = database.getCollection<WorldMap>()
            val world = collection.findOne(Filters.eq(WorldMap::name.name, name))
            return world != null
        } catch (it: java.lang.Exception) {
            println(it.localizedMessage)
            println(it.stackTraceToString())
            throw it
        }
    }

    private fun getSchema(): MongoDatabase =
        MongoDataSource.mongoClient.getDatabase("travel")
}
