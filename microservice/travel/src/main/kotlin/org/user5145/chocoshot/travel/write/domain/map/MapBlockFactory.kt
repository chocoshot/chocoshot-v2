package org.user5145.chocoshot.travel.write.domain.map

import org.user5145.chocoshot.shared.domain.MapBlockType
import org.user5145.chocoshot.travel.write.domain.map.json.Coordinates

object MapBlockFactory{
    fun create(x: Int, y: Int, type: MapBlockType) = MapBlock(arrayListOf(type), Coordinates(x, y))

    fun create(x: Int, y: Int) = MapBlock(ArrayList(), Coordinates(x, y))
}

