package org.user5145.chocoshot.travel.write.domain.map

import org.user5145.chocoshot.travel.write.domain.map.json.TiledJsonConverter

object MapFactory {
    fun newMap(name: String, json: String, tiledJsonConverter: TiledJsonConverter = TiledJsonConverter): WorldMap
        = WorldMap(name = name, blocks = tiledJsonConverter.convert(json))
}