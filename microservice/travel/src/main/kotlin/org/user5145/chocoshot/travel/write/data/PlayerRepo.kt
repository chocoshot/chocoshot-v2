package org.user5145.chocoshot.travel.write.data

import com.mongodb.client.MongoDatabase
import com.mongodb.client.model.Filters
import com.mongodb.client.model.UpdateOptions
import org.litote.kmongo.*
import org.user5145.chocoshot.db.mongo.MongoDataSource
import org.user5145.chocoshot.travel.write.domain.player.Player
import java.sql.SQLException

class PlayerRepo() {
    init {
        val collection = getSchema().getCollection<Player>()
    }

    fun get(id: Id<Any>): Player {
        val database = getSchema()
        val collection = database.getCollection<Player>()
        return collection.findOneById(id) ?: throw SQLException()
    }

    fun get(name: String): Player {
        val database = getSchema()
        val collection = database.getCollection<Player>()
        val result =  collection.findOne(Filters.eq(Player::name.name, name))
        return result ?: throw SQLException()
    }

    fun forEach( function: (Player) -> Unit ) {
        val database = getSchema()
        val collection = database.getCollection<Player>()
        val players = collection.find()
        players.forEach { function.invoke(it) }
    }

    fun upsert(player: Player) {
        val database = getSchema()
        val collection = database.getCollection<Player>()
        val options = UpdateOptions().upsert(true)
        collection.updateOneById(player._id, player, options)
    }

    private fun getSchema(): MongoDatabase =
        MongoDataSource.mongoClient.getDatabase("travel")
}
