package org.user5145.chocoshot.travel.write.domain.map.json

data class TiledLayerDto(
    val data: MutableList<Long> = ArrayList(),
    val name: String,
    val width: Int,
    val height: Int
)