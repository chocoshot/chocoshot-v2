package org.user5145.chocoshot.travel.write.domain.map.json

import java.lang.Exception

class MapCenterNotFoundException : Exception()