package org.user5145.chocoshot.travel.write.event

import org.user5145.chocoshot.travel.write.domain.map.WorldMap

object MapAddedEventDtoFactory{
        fun create(map: WorldMap) = MapAddedEventDto(map.name, map.provideSpawns())
}
