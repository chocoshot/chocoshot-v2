package org.user5145.chocoshot.travel.write.domain.map

import org.user5145.chocoshot.travel.write.data.WorldMapRepo

//TODO
object CachedMap {
    var maps: MutableList<WorldMap> = WorldMapRepo().get()
        private set
        get() {
            refresh()
            return field
        }

    fun refresh(){
        maps = WorldMapRepo().get()
    }
}