package org.user5145.chocoshot.travel.write.domain.map.json

data class TiledMapRootDto(
    val layers: MutableList<TiledLayerDto> = ArrayList(),
)