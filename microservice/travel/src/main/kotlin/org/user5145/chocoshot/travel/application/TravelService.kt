package org.user5145.chocoshot.travel.application

import io.grpc.stub.StreamObserver
import mu.KotlinLogging
import org.user5145.chocoshot.amqp.kafka.PushEvent
import org.user5145.chocoshot.amqp.kafka.ServerId
import org.user5145.chocoshot.json.extension.toJson
import org.user5145.chocoshot.shared.rpc.*
import org.user5145.chocoshot.travel.write.data.PlayerRepo
import org.user5145.chocoshot.travel.write.data.WorldMapRepo
import org.user5145.chocoshot.travel.write.domain.map.CachedMap
import org.user5145.chocoshot.travel.write.domain.map.MapFactory
import org.user5145.chocoshot.travel.write.domain.map.WorldMap
import org.user5145.chocoshot.travel.write.event.MapAddedEventDtoFactory
import org.user5145.chocoshot.travel.write.event.TravelTopicIds
import shared.Shared.ChampionName
import shared.Shared.Empty
import org.user5145.chocoshot.travel.write.domain.map.json.Coordinates as RequestCoordinates

class TravelService(val playerRepo: PlayerRepo = PlayerRepo(),
                    val mapRepo: WorldMapRepo = WorldMapRepo(),
                    val mapFactory: MapFactory = MapFactory,
                    val mapAddedEventDtoFactory: MapAddedEventDtoFactory = MapAddedEventDtoFactory,
                    val eTADtoFactory: ETADtoFactory = ETADtoFactory,
                    val coordinatesFactory: CoordinatesDtoFactory = CoordinatesDtoFactory) : TravelServiceGrpc.TravelServiceImplBase() {
    val logger = KotlinLogging.logger {}

    override fun addToQueue(request: NewAction, responseObserver: StreamObserver<ETA>) {
        runLoggingAndHandlingError(request, responseObserver) {
            val player = playerRepo.get(request.name.name)
            val map = mapRepo.get(player.mapName)
            val eta = player.goTo(map = map, destination = RequestCoordinates(request.coords.x, request.coords.y))
            playerRepo.upsert(player)
            responseObserver.onNext(eTADtoFactory.create(eta))
        }
    }

    override fun getPlayerLocation(request: ChampionName, responseObserver: StreamObserver<Coordinates>) {
        runLoggingAndHandlingError(request, responseObserver) {
            val player = playerRepo.get(request.name)
            player.refreshQueue()
            playerRepo.upsert(player)
            responseObserver.onNext(coordinatesFactory.create(player.coordiantes))
        }
    }

    override fun getPlayerETA(request: ChampionName, responseObserver: StreamObserver<ETA>) {
        runLoggingAndHandlingError(request, responseObserver) {
            val player = playerRepo.get(request.name)
            val eta = player.getEta()
            responseObserver.onNext(eTADtoFactory.create(eta))
        }
    }

    override fun getQueue(request: ChampionName, responseObserver: StreamObserver<ActionQueue>) {
        runLoggingAndHandlingError(request, responseObserver) {
            val player = playerRepo.get(request.name)
            player.refreshQueue()
            val queue = player.actionQueue
            val limit = player.queueLimit
            playerRepo.upsert(player)
            responseObserver.onNext(ActionQueueDtoFactory.create(limit, queue))
        }
    }

    override fun removeLastFromQueue(request: ChampionName, responseObserver: StreamObserver<Empty>) {
        runLoggingAndHandlingError(request, responseObserver) {
            val player = playerRepo.get(request.name)
            player.removeLastFromQueue()
            playerRepo.upsert(player)
            responseObserver.onNext(Empty.getDefaultInstance())
        }
    }

    override fun addMap(request: GameMap, responseObserver: StreamObserver<Empty>) {
        logger.debug { "received: ${request.name}" }
        runCatching {
            lateinit var map: WorldMap
            if ( mapRepo.exists(request.name) ) {
                map = mapRepo.get(request.name)
                map.replace(request.json)
                mapRepo.upsert(map)
            } else {
                map = mapFactory.newMap(request.name, request.json)
                mapRepo.upsert(map)
            }
            CachedMap.refresh()
            PushEvent.push(topic = TravelTopicIds.MAP_ADDED.name, key = "1.0", data = mapAddedEventDtoFactory.create(map).toJson(), ServerId.TRAVEL.name)
            responseObserver.onNext(Empty.getDefaultInstance())
        }.onSuccess {
            responseObserver.onCompleted()
        }.onFailure {
            logger.warn { it.stackTraceToString() }
            responseObserver.onError(it)
        }
    }

    override fun teleportAllChampions(request: Empty, responseObserver: StreamObserver<Empty>) {
        runLoggingAndHandlingError(request, responseObserver){
            playerRepo.forEach {
                it.resetPosition()
                playerRepo.upsert(it)
            }
            responseObserver.onNext(Empty.getDefaultInstance())
        }
    }

    private fun <T : Any, U: Any, W: Any> runLoggingAndHandlingError(request: U, responseObserver: StreamObserver<T>, function: () -> W, ): W? {
        logger.debug { "received: ${request.toJson()}" }
        var result: W? = null
        runCatching {
            result = function.invoke()
        }.onFailure {
            logger.warn { it.stackTraceToString() }
            responseObserver.onError(it)
        }.onSuccess {
            responseObserver.onCompleted()
        }
        return result
    }
}

