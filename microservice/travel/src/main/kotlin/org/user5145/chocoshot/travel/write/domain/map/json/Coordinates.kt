package org.user5145.chocoshot.travel.write.domain.map.json

data class Coordinates(val x: Int, val y: Int)
