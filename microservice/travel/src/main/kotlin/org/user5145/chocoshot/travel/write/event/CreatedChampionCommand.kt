package org.user5145.chocoshot.travel.write.event

import mu.KotlinLogging
import org.user5145.chocoshot.json.extension.jsonToObject
import org.user5145.chocoshot.shared.event.account.AddChampionEventDto
import org.user5145.chocoshot.travel.write.data.PlayerRepo
import org.user5145.chocoshot.travel.write.data.WorldMapRepo
import org.user5145.chocoshot.travel.write.domain.map.WorldMap
import org.user5145.chocoshot.travel.write.domain.player.PlayerFactory

class CreatedChampionCommand {
    companion object {
        private val logger = KotlinLogging.logger {}

        fun createChampion(
            json: String,
            repo: PlayerRepo,
            mapRepo: WorldMapRepo,
            factory: PlayerFactory
        ) {
            logger.debug { "processing event in the travel module: ${json}" }
            val playerDto: AddChampionEventDto = json.jsonToObject()
            val mapDto: WorldMap = mapRepo.get("default map")
            val aggregate = factory.newPlayer(name = playerDto.name, mapName = mapDto.name, spawns = mapDto.provideSpawns())
            repo.upsert(aggregate)
        }
    }
}