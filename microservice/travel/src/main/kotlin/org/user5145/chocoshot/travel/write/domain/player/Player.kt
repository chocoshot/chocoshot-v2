package org.user5145.chocoshot.travel.write.domain.player

import com.fasterxml.jackson.annotation.JsonIgnore
import com.google.common.collect.ImmutableList
import io.ktor.util.*
import io.ktor.util.collections.*
import org.bson.types.ObjectId
import org.user5145.chocoshot.amqp.kafka.PushEvent
import org.user5145.chocoshot.amqp.kafka.ServerId
import org.user5145.chocoshot.json.extension.toJson
import org.user5145.chocoshot.shared.domain.MapBlockType
import org.user5145.chocoshot.shared.event.account.RegistrationTopicIds
import org.user5145.chocoshot.shared.event.account.SteppedOnBlockEventDto
import org.user5145.chocoshot.travel.write.domain.algorithm.AStar
import org.user5145.chocoshot.travel.write.domain.algorithm.Coords
import org.user5145.chocoshot.travel.write.domain.map.CachedMap
import org.user5145.chocoshot.travel.write.domain.map.WorldMap
import org.user5145.chocoshot.travel.write.domain.map.json.Coordinates
import java.util.*

class Player @KtorExperimentalAPI constructor(
    val _id: ObjectId = ObjectId(),
    val name: String,
    var coordiantes: Coordinates,
    actionQueue: List<Action> = ConcurrentList<Action>().toMutableList(),
    val mapName: String = "default",
    var queueLimit: Int = 3
) {
    var actionQueue: List<Action> = actionQueue
        get() = ImmutableList.copyOf(field)

    fun goTo(destination: Coordinates, map: WorldMap, algorithm: AStar = AStar): Long {
        if (isQueueFull())
            throw QueueFull()
        val coords = if (actionQueue.isEmpty())
             Coords(coordiantes.x, coordiantes.y)
        else {
            val lastCoords = actionQueue.last().coordinates
            Coords(lastCoords.x, lastCoords.y)
        }

        val path = algorithm.getPath(
            start = coords,
            end = Coords(destination.x, destination.y),
            map = map.blocksAsArray()
        )

        val time = pathToETA(path)

        val eTA: Long = if (getTime() > getEta()) {
            getTime() + time
        } else {
            val timeToAdd = getEta() - getTime()
            getTime() + time + timeToAdd
        }

        actionQueue = actionQueue.plus(Action(eTA = eTA, coordinates = destination))
        return eTA
    }

    @JsonIgnore
    fun getEta() : Long {
        return actionQueue.maxByOrNull(Action::eTA)?.eTA ?: 0
    }

    @JsonIgnore
    fun isQueueFull(): Boolean {
        refreshQueue()
        return actionQueue.size >= queueLimit
    }

    fun removeLastFromQueue(){
        refreshQueue()
        if (actionQueue.isNotEmpty())
            actionQueue = actionQueue.dropLast(1)
        else
            throw NothingToDelete()
    }

    fun resetPosition(){
        refreshQueue()
        val coords = CachedMap.maps.first().getBlockWithEffect(MapBlockType.SPAWN).first()
        coordiantes = Coordinates(coords.x, coords.y)
        actionQueue = actionQueue.drop( actionQueue.size )
    }

    fun refreshQueue(){
        val done = actionQueue.filter { it.eTA < getTime()  }
        val last = done.maxByOrNull(Action::eTA)
        if (last != null){
            coordiantes = last.coordinates
            val blockEffects = CachedMap.maps.first().getEffects(last.coordinates.x, last.coordinates.y)
            sendSteppedOnBlock(name, blockEffects)
        }
        actionQueue = actionQueue.drop( done.size )
    }

    private fun getTime(): Long {
        return Calendar.getInstance().timeInMillis  / 1000
    }

    private fun pathToETA(path: List<Coords>): Long {
        var eta = 0L
        var previous = path.first()
        for (block in path) {
            if (previous == block)
                continue

            eta += if (previous.x != block.x && previous.y != block.y) 14 else 10
            previous = block
        }
        return eta
    }

    private fun sendSteppedOnBlock(who: String, blockEffects: MutableList<MapBlockType>) {
        val dto = SteppedOnBlockEventDto( who, blockEffects )
        PushEvent.push(topic = RegistrationTopicIds.STEPPED_ON_BLOCK.name,
            key = "1.1",
            data = dto.toJson(),
            id = ServerId.TRAVEL.name)
    }
}