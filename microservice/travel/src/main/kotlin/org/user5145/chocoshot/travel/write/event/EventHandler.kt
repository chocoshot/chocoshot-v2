package org.user5145.chocoshot.travel.write.event

import org.user5145.chocoshot.amqp.kafka.EventNotMappedException
import org.user5145.chocoshot.amqp.kafka.ServerId
import org.user5145.chocoshot.amqp.kafka.Subscribe
import org.user5145.chocoshot.shared.event.account.RegistrationTopicIds
import org.user5145.chocoshot.travel.write.data.PlayerRepo
import org.user5145.chocoshot.travel.write.data.WorldMapRepo
import org.user5145.chocoshot.travel.write.domain.player.PlayerFactory
import org.user5145.chocoshot.travel.write.event.CreatedChampionCommand.Companion.createChampion

object EventHandler {
    val playerRepo = PlayerRepo()
    val mapRepo = WorldMapRepo()

    fun start(){
        addChampionCommand()
    }

    private fun addChampionCommand()  {
        Subscribe().subscribe(ServerId.REGISTRATION.name, RegistrationTopicIds.CREATED_CHAMPION.name) {
            when (it.key()) {
                "1.0" -> { createChampion(it.value(), playerRepo, mapRepo, PlayerFactory) }
                else -> { throw EventNotMappedException() }
            }
        }
    }
}