package org.user5145.chocoshot.travel

import mu.KotlinLogging
import org.user5145.chocoshot.grpc.GrpcServer
import org.user5145.chocoshot.grpc.ServerPort
import org.user5145.chocoshot.travel.application.TravelService
import org.user5145.chocoshot.travel.write.event.EventHandler

class Main() {
    private val log = KotlinLogging.logger {}

    fun main(){
        EventHandler.start()
        GrpcServer.start(TravelService(), ServerPort.TRAVEL_SERVICE)
    }

    fun shutdown() {
        GrpcServer.shutdown()
    }
}
