package org.user5145.chocoshot.travel.write.domain.algorithm

data class Coords(val x: Int, val y: Int, var blocked: Boolean = false)
