package org.user5145.chocoshot.travel.write.event

import org.user5145.chocoshot.travel.write.domain.map.MapBlock

data class MapAddedEventDto(val name: String, val spawns: List<MapBlock>)
