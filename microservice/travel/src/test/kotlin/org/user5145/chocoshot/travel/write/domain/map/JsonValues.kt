package org.user5145.chocoshot.travel.write.domain.map

enum class JsonValues(val value: String) {
    TILED_MAP(this::class.java.getResource("/TiledMap.json").readText())
}