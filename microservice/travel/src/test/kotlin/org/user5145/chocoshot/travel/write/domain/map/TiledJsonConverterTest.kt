package org.user5145.chocoshot.travel.write.domain.map

import org.user5145.chocoshot.travel.write.domain.map.json.TiledJsonConverter
import kotlin.test.Test
import kotlin.test.assertNotNull

class TiledJsonConverterTest {

    @Test
    fun testConversion() {
        //given
        val result = JsonValues.TILED_MAP.value
        //when
        val dto = TiledJsonConverter.convert(result)
        //then
        assertNotNull(dto)
    }
}