package org.user5145.chocoshot.travel

import org.user5145.chocoshot.travel.write.domain.algorithm.AStar
import org.user5145.chocoshot.travel.write.domain.algorithm.Coords
import kotlin.test.Test
import kotlin.test.assertEquals

class AStarTest{
    val aStar: AStar = AStar

    @Test
    fun testAStar(){
        //given
        val map = arrayOf(
            Coords(0,0), Coords(1,0, false), Coords(2,0, false), Coords(3,0, false), Coords(4,0, false), Coords(5,0, false), Coords(6,0, false),
            Coords(0,1), Coords(1,1, true ), Coords(2,1, true ), Coords(3,1, true ), Coords(4,1, true ), Coords(5,1, false), Coords(6,1, false),
            Coords(0,2), Coords(1,2, false), Coords(2,2, false), Coords(3,2, false), Coords(4,2, true ), Coords(5,2, false), Coords(6,2, false),
            Coords(0,3), Coords(1,3, false), Coords(2,3, false), Coords(3,3, false), Coords(4,3, true ), Coords(5,3, false), Coords(6,3, false),
            Coords(0,4), Coords(1,4, false), Coords(2,4, false), Coords(3,4, false), Coords(4,4, true ), Coords(5,4, false), Coords(6,4, false),
            Coords(0,5), Coords(1,5, false), Coords(2,5, false), Coords(3,5, false), Coords(4,5, true ), Coords(5,5, false), Coords(6,5, false),
            Coords(0,6), Coords(1,6, false), Coords(2,6, false), Coords(3,6, false), Coords(4,6, true ), Coords(5,6, true ), Coords(6,6, false),
            Coords(0,7), Coords(1,7, false), Coords(2,7, false), Coords(3,7, false), Coords(4,7, false), Coords(5,7, false), Coords(6,7, false),
        )

        //when
        val actual = aStar.getPath(start = Coords(1,5),
                end = Coords(5,5),
                map)

        //then
        val expected = arrayListOf(Coords(x=5, y=5, blocked=false), Coords(x=6, y=6, blocked=false), Coords(x=5, y=7, blocked=false), Coords(x=4, y=7, blocked=false), Coords(x=3, y=6, blocked=false), Coords(x=2, y=5, blocked=false), Coords(x=1, y=5, blocked=false))
        assertEquals(expected, actual)
    }
}