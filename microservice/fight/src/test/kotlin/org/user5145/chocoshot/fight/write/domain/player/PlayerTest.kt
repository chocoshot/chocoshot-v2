package org.user5145.chocoshot.fight.write.domain.player

import org.junit.Assert
import kotlin.test.Test

class PlayerTest {

    @Test
    fun testGetStatHp(){
        //given
        val player = PlayerRecipe(name = "johnny", level = 50, stats = StatsRatio(0.1f, 0.9f))

        //when
        val hpCount = player.getStat(StatsRatio::hp);

        //then
        Assert.assertEquals(hpCount, 500)
    }

    @Test
    fun testGetStatStrength(){
        //given
        val player = PlayerRecipe(name = "johnny", level = 50, stats = StatsRatio(0.1f, 0.9f))

        //when
        val strengthCount = player.getStat(StatsRatio::strength);

        //then
        Assert.assertEquals(strengthCount, 4500)
    }

    @Test
    fun testLevelUp(){
        //given
        val player = PlayerRecipe(name = "johnny", level = 1, stats = StatsRatio(0.1f, 0.9f))

        //when
        player.addExperience(10001);

        //then
        Assert.assertEquals(player.experience, 10001)
        Assert.assertEquals(player.level, 11)
    }

    @Test(expected = WrongParameters::class)
    fun testChangeStatsToTooSmallNumber(){
        //given
        val player = PlayerRecipe(name = "johnny", level = 1, stats = StatsRatio(0.1f, 0.9f))

        //when
        player.changeStats(0.0f, 0.0f);
    }

    @Test
    fun testChangeStats(){
        //given
        val player = PlayerRecipe(name = "johnny", level = 1, stats = StatsRatio(0.1f, 0.9f))

        //when
        player.changeStats(0.4f, 0.6f);

        //given
        Assert.assertEquals(player.stats, StatsRatio(0.6f, 0.4f))
    }

    @Test
    fun testCreatePlayer(){
        //given
        val playerRecipe = PlayerRecipe(name = "johnny", level = 1, stats = StatsRatio(0.1f, 0.9f))

        //when
        val player = playerRecipe.createPlayer()

        //given
        Assert.assertEquals(player, Player(hp = 10, strength = 90))
    }
}