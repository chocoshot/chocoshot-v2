package org.user5145.chocoshot.fight.write.domain.enemy

import org.junit.Assert
import org.user5145.chocoshot.fight.write.domain.player.PlayerRecipe
import org.user5145.chocoshot.fight.write.domain.player.StatsRatio
import kotlin.test.Test

class EnemyTest {

    @Test
    fun testLevelIsNotToBigOrSmall(){
        //given
        val enemyRecipe = EnemyRecipe(name = "johnny", strengthMax = 10, strengthMin = 9, hpMax = 3, hpMin = 1, levelMin = 1, levelMax = 99 )
        val playerRecipe = PlayerRecipe(name = "johnny", level = 10, stats = StatsRatio(0.1f, 0.9f))

        //when
        val enemy = enemyRecipe.createEnemy( playerRecipe );

        //then
        Assert.assertTrue(enemy.level > 8 && enemy.level < 13)
    }

    @Test
    fun testHpIsNotToBigOrSmall(){
        //given
        val enemyRecipe = EnemyRecipe(name = "johnny", strengthMax = 10, strengthMin = 9, hpMax = 3, hpMin = 1, levelMin = 1, levelMax = 99 )
        val playerRecipe = PlayerRecipe(name = "johnny", level = 10, stats = StatsRatio(0.1f, 0.9f))

        //when
        val enemy = enemyRecipe.createEnemy( playerRecipe );

        //then
        Assert.assertTrue(enemy.hp >= enemyRecipe.hpMin && enemy.hp <= enemyRecipe.hpMax)
    }
}