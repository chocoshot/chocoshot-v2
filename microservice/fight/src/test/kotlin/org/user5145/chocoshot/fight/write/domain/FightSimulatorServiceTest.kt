package org.user5145.chocoshot.fight.write.domain

import org.junit.Assert
import org.user5145.chocoshot.fight.write.domain.enemy.EnemyRecipe
import org.user5145.chocoshot.fight.write.domain.player.PlayerRecipe
import org.user5145.chocoshot.fight.write.domain.player.StatsRatio
import kotlin.test.Test

class FightSimulatorServiceTest {
    val simulatorService = FightSimulatorService()

    @Test
    fun testVictory(){
        //given
        val player = PlayerRecipe(name = "johnny", level = 50, stats = StatsRatio(0.1f, 0.9f))
        val enemy = EnemyRecipe(name = "rat", hpMin = 1, hpMax = 2, strengthMin = 1, strengthMax = 2)

        //when
        val fightScore = simulatorService.simulateFight(enemy, player)

        //then
        Assert.assertTrue(fightScore.success)
    }

    @Test
    fun testLost(){
        //given
        val player = PlayerRecipe(name = "johnny", level = 1, stats = StatsRatio(0.9f, 0.1f))
        val enemy = EnemyRecipe(name = "rat", hpMin = 1000, hpMax = 1001, strengthMin = 100, strengthMax = 101)

        //when
        val fightScore = simulatorService.simulateFight(enemy, player)

        //then
        Assert.assertTrue( fightScore.success.not() )
    }

    @Test
    fun testNegativeHpForEnemy(){
        //given
        val player = PlayerRecipe(name = "johnny", level = 50, stats = StatsRatio(0.1f, 0.9f))
        val enemy = EnemyRecipe(name = "rat", hpMin = 1, hpMax = 2, strengthMin = 1, strengthMax = 2)

        //when
        val fightScore = simulatorService.simulateFight(enemy, player)

        //then
        Assert.assertTrue( fightScore.turns.last().culpritHp == 0 )
    }

    @Test
    fun testNegativeHpForPlayer(){
        //given
        val player = PlayerRecipe(name = "johnny", level = 1, stats = StatsRatio(0.9f, 0.1f))
        val enemy = EnemyRecipe(name = "rat", hpMin = 1000, hpMax = 1001, strengthMin = 100, strengthMax = 101)

        //when
        val fightScore = simulatorService.simulateFight(enemy, player)

        //then
        Assert.assertTrue( fightScore.turns.last().attackerHp == 0 )
    }

    @Test
    fun testNeverEndingFight(){
        //given
        val player = PlayerRecipe(name = "johnny", level = 50, stats = StatsRatio(0.9f, 0.1f))
        val enemy = EnemyRecipe(name = "rat", hpMin = 100000, hpMax = 100001, strengthMin = 1, strengthMax = 2)

        //when
        val fightScore = simulatorService.simulateFight(enemy, player)

        //then
        Assert.assertTrue( fightScore.turns.size == 100 )
        Assert.assertTrue( fightScore.success.not() )
    }
}