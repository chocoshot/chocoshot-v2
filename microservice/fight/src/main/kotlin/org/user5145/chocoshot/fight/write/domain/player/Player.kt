package org.user5145.chocoshot.fight.write.domain.player

data class Player(var hp: Int, var strength: Int) {
    fun getDamage(): Int {
        return strength / 2
    }
}