package org.user5145.chocoshot.fight.write.domain.player

data class Fight(val success: Boolean, val level: Int, val experience: Long, val timestamp: Long, val turns: List<Turn>)

data class Turn(val attacker: String,
                val culprit: String,
                val culpritHp: Int,
                val culpritDamageDone: Int,
                val attackerHp: Int,
                val attackerDamageDone: Int,
                val whoIsPlaying: String)