package org.user5145.chocoshot.fight.write.domain

import org.user5145.chocoshot.fight.write.domain.enemy.Enemy
import org.user5145.chocoshot.fight.write.domain.enemy.EnemyRecipe
import org.user5145.chocoshot.fight.write.domain.player.Fight
import org.user5145.chocoshot.fight.write.domain.player.Player
import org.user5145.chocoshot.fight.write.domain.player.PlayerRecipe
import org.user5145.chocoshot.fight.write.domain.player.Turn

class FightSimulatorService {
    fun simulateFight(enemyRecipe: EnemyRecipe, playerRecipe: PlayerRecipe): Fight {
        val enemy = enemyRecipe.createEnemy(playerRecipe)
        val player = playerRecipe.createPlayer()
        var success = false

        val result = ArrayList<Turn>()
        for (i in 1..100) {
            result.add(playTurn(i, playerRecipe, player, enemy))
            if ( result.last().culpritHp == 0 ){
                success = true
                break;
            } else if (result.last().attackerHp == 0) {
                break;
            }
        }
        addExp(success, playerRecipe)
        return Fight(success, playerRecipe.level, playerRecipe.experience, getTimestamp(), result);
    }

    private fun getTimestamp(): Long {
        return System.currentTimeMillis()
    }

    private fun addExp(success: Boolean, playerRecipe: PlayerRecipe) {
        if (success)
            playerRecipe.addExperience(100)
        else
            playerRecipe.addExperience(25)
    }


    private fun playTurn(index: Int, playerRecipe: PlayerRecipe, player: Player, enemy: Enemy): Turn {
        return if (index % 2 == 1){
            player.hp -= enemy.getDamage()
            if (player.hp < 0)
                player.hp = 0
            Turn( playerRecipe.name, enemy.name, enemy.hp, enemy.getDamage(), player.hp, 0, enemy.name )
        } else {
            enemy.hp -= player.getDamage()
            if (enemy.hp < 0)
                enemy.hp = 0
            Turn( playerRecipe.name, enemy.name, enemy.hp, 0, player.hp, player.getDamage(), playerRecipe.name )
        }
    }
}