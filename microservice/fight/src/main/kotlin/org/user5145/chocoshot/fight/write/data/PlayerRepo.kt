package org.user5145.chocoshot.fight.write.data

import com.mongodb.client.MongoDatabase
import com.mongodb.client.model.Filters
import com.mongodb.client.model.UpdateOptions
import org.litote.kmongo.findOne
import org.litote.kmongo.getCollection
import org.litote.kmongo.updateOneById
import org.user5145.chocoshot.db.mongo.MongoDataSource
import org.user5145.chocoshot.fight.write.domain.player.PlayerRecipe
import java.sql.SQLException

class PlayerRepo {
    init {
        val collection = getSchema().getCollection<PlayerRecipe>()
    }

    fun get(name: String): PlayerRecipe {
        val database = getSchema()
        val collection = database.getCollection<PlayerRecipe>()
        val result =  collection.findOne(Filters.eq(PlayerRecipe::name.name, name))
        return result ?: throw SQLException()
    }

    fun upsert(playerRecipe: PlayerRecipe) {
        val database = getSchema()
        val collection = database.getCollection<PlayerRecipe>()
        val options = UpdateOptions().upsert(true)
        collection.updateOneById(playerRecipe._id, playerRecipe, options)
    }

    private fun getSchema(): MongoDatabase =
        MongoDataSource.mongoClient.getDatabase("fight")
}