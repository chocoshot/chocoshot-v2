package org.user5145.chocoshot.fight.write.domain.enemy

import org.bson.types.ObjectId
import org.user5145.chocoshot.fight.write.domain.player.PlayerRecipe
import kotlin.random.Random

data class EnemyRecipe(val _id: ObjectId = ObjectId(),
                       val name: String,
                       var hpMin: Int,
                       var hpMax: Int,
                       var strengthMin: Int,
                       var strengthMax: Int,
                       var levelMin: Int = 1,
                       var levelMax: Int = 99,
) {

    fun createEnemy(player: PlayerRecipe): Enemy{
        val hp = Random.nextInt(hpMax - hpMin) + hpMin
        val strength =  Random.nextInt(strengthMax - strengthMin) + strengthMin
        return Enemy(name, hp, strength, randomizeTo10Percent(player.level))
    }

    fun randomizeTo10Percent(value: Int): Int {
        val percentage = (90..110).random()
        return (value * percentage / 100) + 1
    }

}
