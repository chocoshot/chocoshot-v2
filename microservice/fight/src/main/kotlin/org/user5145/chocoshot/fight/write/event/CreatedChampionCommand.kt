package org.user5145.chocoshot.fight.write.event

import mu.KotlinLogging
import org.user5145.chocoshot.fight.write.data.PlayerRepo
import org.user5145.chocoshot.fight.write.domain.player.PlayerRecipeFactory
import org.user5145.chocoshot.json.extension.jsonToObject
import org.user5145.chocoshot.shared.event.account.AddChampionEventDto

object CreatedChampionCommand {
        private val logger = KotlinLogging.logger {}

        fun createChampion(
            json: String,
            repo: PlayerRepo = PlayerRepo(),
            factory: PlayerRecipeFactory = PlayerRecipeFactory
        ) {
            logger.debug { "processing event in the fight module: $json" }
            val playerDto: AddChampionEventDto = json.jsonToObject()
            val aggregate = factory.create(playerDto.name)
            repo.upsert(aggregate)
        }
}