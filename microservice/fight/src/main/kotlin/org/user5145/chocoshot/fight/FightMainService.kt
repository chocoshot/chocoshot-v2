package org.user5145.chocoshot.fight

import org.user5145.chocoshot.fight.application.FightService
import org.user5145.chocoshot.grpc.GrpcServer
import org.user5145.chocoshot.grpc.ServerPort

class FightMainService {
    fun main(){
        EventHandler().start()
        GrpcServer.start(FightService(), ServerPort.FIGHT_SERVICE)
    }

    fun shutdown() {
        GrpcServer.shutdown()
    }
}