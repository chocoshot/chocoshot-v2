package org.user5145.chocoshot.fight.write.domain.player

data class Coords(val x: Int, val y: Int)