package org.user5145.chocoshot.fight.write.domain.enemy


data class Enemy(val name: String, var hp: Int = 100, var strength: Int = 10, var level: Int = 1){
    fun getDamage(): Int {
        return strength / 2
    }
}
