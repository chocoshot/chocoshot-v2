package org.user5145.chocoshot.fight.application

import org.user5145.chocoshot.fight.write.domain.player.Fight
import org.user5145.chocoshot.fight.write.domain.player.Turn
import org.user5145.chocoshot.shared.rpc.FightResult
import org.user5145.chocoshot.shared.rpc.FightTurn
import org.user5145.chocoshot.shared.rpc.Fights

object FightsDtoFactory {
    fun create(fight: List<Fight>): Fights {
        val builder = Fights.newBuilder()
        val grpcFights = fight.mapIndexed { index, action -> action.mapFightsToGrpcFights(index) }
        builder.addAllFights(grpcFights)
        return builder.build()
    }

    private fun Fight.mapFightsToGrpcFights(index: Int): FightResult {
        val grpcTurns = turns.mapIndexed { index, action -> action.mapTurnsToGrpcTurns(index) }
        val result = FightResult.newBuilder()
            .setId(index)
            .setTimestamp(timestamp)
            .setExperience(experience)
            .setLevel(level)
            .setSuccess(success)
            .addAllHistory(grpcTurns)
            .build()
        return result
    }

    private fun Turn.mapTurnsToGrpcTurns(index: Int): FightTurn {
        val turn = FightTurn.newBuilder()
            .setId(index)
            .setAttacker(attacker)
            .setCulprit(culprit)
            .setCulpritDamageDone(culpritDamageDone)
            .setAttackerHp(attackerHp)
            .setCulpritHp(culpritHp)
            .setAttackerDamageDone(attackerDamageDone)
            .setWhoIsPlaying(whoIsPlaying)
            .build()
        return turn
    }
}