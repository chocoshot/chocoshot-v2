package org.user5145.chocoshot.fight.write.data

import com.mongodb.client.MongoDatabase
import com.mongodb.client.model.Filters
import com.mongodb.client.model.IndexOptions
import com.mongodb.client.model.Indexes
import com.mongodb.client.model.UpdateOptions
import org.bson.conversions.Bson
import org.litote.kmongo.findOne
import org.litote.kmongo.getCollection
import org.litote.kmongo.updateOneById
import org.user5145.chocoshot.db.mongo.MongoDataSource
import org.user5145.chocoshot.fight.write.domain.enemy.EnemyRecipe
import java.sql.SQLException

class EnemyRepo {
    init {
        val collection = getSchema().getCollection<EnemyRecipe>()
        val indexOptions: IndexOptions = IndexOptions().unique(true)
        collection.createIndex(Indexes.ascending(EnemyRecipe::name.name), indexOptions)
    }

    fun getAny(filter: Bson): EnemyRecipe {
        val database = getSchema()
        val collection = database.getCollection<EnemyRecipe>()
        val result =  collection.findOne( filter )
        return result ?: throw SQLException()
    }

    fun <T: Any> doForAll(function: (EnemyRecipe) -> T): List<T> {
        val database = getSchema()
        val collection = database.getCollection<EnemyRecipe>()
        val result =  collection.find().map { function(it) }.toList();
        return result
    }

    fun upsert(enemyRecipe: EnemyRecipe) {
        val database = getSchema()
        val collection = database.getCollection<EnemyRecipe>()
        val options = UpdateOptions().upsert(true)
        collection.updateOneById(enemyRecipe._id, enemyRecipe, options)
    }

    fun delete(name: String) {
        val database = getSchema()
        val collection = database.getCollection<EnemyRecipe>()
        collection.deleteOne(Filters.eq(EnemyRecipe::name.name, name))
    }

    private fun getSchema(): MongoDatabase =
        MongoDataSource.mongoClient.getDatabase("fight")
}