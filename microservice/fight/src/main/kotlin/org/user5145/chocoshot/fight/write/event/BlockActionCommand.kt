package org.user5145.chocoshot.fight.write.event

import mu.KotlinLogging
import org.user5145.chocoshot.fight.write.data.EnemyRepo
import org.user5145.chocoshot.fight.write.data.PlayerRepo
import org.user5145.chocoshot.fight.write.domain.FightSimulatorService
import org.user5145.chocoshot.json.extension.jsonToObject
import org.user5145.chocoshot.shared.domain.MapBlockType
import org.user5145.chocoshot.shared.event.account.SteppedOnBlockEventDto

object BlockActionCommand {
        private val logger = KotlinLogging.logger {}

        fun executeAction(
            json: String,
            playerRepo: PlayerRepo = PlayerRepo(),
            enemyRepo: EnemyRepo = EnemyRepo(),
            fightService: FightSimulatorService = FightSimulatorService(),
        ) {
            logger.debug { "processing event in the fight module: $json" }
            val effectsDto: SteppedOnBlockEventDto = json.jsonToObject()
            if (effectsDto.effects.contains(MapBlockType.ENEMY)) {
                logger.debug("user ${effectsDto.name} stepped on enemy block")
                val player = playerRepo.get(effectsDto.name)
                player.attack(enemyRepo, fightService)
                playerRepo.upsert(player)
            }
        }
}