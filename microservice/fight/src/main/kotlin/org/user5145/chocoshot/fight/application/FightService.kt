package org.user5145.chocoshot.fight.application

import io.grpc.stub.StreamObserver
import mu.KotlinLogging
import org.user5145.chocoshot.fight.write.data.EnemyRepo
import org.user5145.chocoshot.fight.write.data.PlayerRepo
import org.user5145.chocoshot.fight.write.domain.FightSimulatorService
import org.user5145.chocoshot.fight.write.domain.enemy.EnemyRecipe
import org.user5145.chocoshot.json.extension.toJson
import org.user5145.chocoshot.shared.rpc.*
import shared.Shared

class FightService(
    val playerRepo: PlayerRepo = PlayerRepo(),
    val enemyRepo: EnemyRepo = EnemyRepo(),
    val fightsFactory: FightsDtoFactory = FightsDtoFactory,
    val fightSimulator: FightSimulatorService = FightSimulatorService(),
    val enemyRecipeFactory: EnemyRecipeFactory = EnemyRecipeFactory,
    val enemyFactory: EnemyFactory = EnemyFactory
): FightServiceGrpc.FightServiceImplBase() {
    val logger = KotlinLogging.logger {}

    override fun attack(request: Shared.ChampionName, responseObserver: StreamObserver<Shared.Empty>) {
        runLoggingAndHandlingError(request, responseObserver) {
            val player = playerRepo.get( request.name )
            player.attack( enemyRepo,  fightSimulator )
            playerRepo.upsert( player )
            responseObserver.onNext( Shared.Empty.getDefaultInstance() )
        }
    }

    override fun getHistory(request: Shared.ChampionName, responseObserver: StreamObserver<Fights>) {
        runLoggingAndHandlingError( request, responseObserver ) {
            val player = playerRepo.get( request.name )
            val history = player.getLatestHistory()
            responseObserver.onNext( fightsFactory.create(history) )
        }
    }

    override fun setStats(request: StatsRatio, responseObserver: StreamObserver<Shared.Empty>) {
        runLoggingAndHandlingError( request, responseObserver ) {
            val player = playerRepo.get( request.name.name )
            player.changeStats(request.strength, request.hp)
            playerRepo.upsert( player )
            responseObserver.onNext( Shared.Empty.getDefaultInstance() )
        }
    }

    override fun addEnemy(request: Enemy, responseObserver: StreamObserver<Shared.Empty>) {
        runLoggingAndHandlingError( request, responseObserver ) {
            enemyRepo.upsert(enemyRecipeFactory.create(request))
            responseObserver.onNext( Shared.Empty.getDefaultInstance() )
        }
    }

    override fun removeEnemy(request: EnemyName, responseObserver: StreamObserver<Shared.Empty>) {
        runLoggingAndHandlingError( request, responseObserver ) {
            enemyRepo.delete(request.value)
            responseObserver.onNext( Shared.Empty.getDefaultInstance() )
        }
    }

    override fun getEnemies(request: Shared.Empty, responseObserver: StreamObserver<Enemies>) {
        runLoggingAndHandlingError( request, responseObserver ) {
            val enemies = enemyRepo.doForAll{ enemyFactory.create(it) }
            val transformedEnemies = Enemies.newBuilder().addAllEnemies(enemies).build()
            responseObserver.onNext( transformedEnemies )
        }
    }

    private fun <T : Any, U: Any, W: Any> runLoggingAndHandlingError(request: U, responseObserver: StreamObserver<T>, function: () -> W, ): W? {
        logger.debug { "received: ${request.toJson()}" }
        var result: W? = null
        runCatching {
            result = function.invoke()
        }.onFailure {
            logger.warn { it.stackTraceToString() }
            responseObserver.onError(it)
        }.onSuccess {
            responseObserver.onCompleted()
        }
        return result
    }


}

object EnemyFactory {
    fun create(enemy: EnemyRecipe): Enemy{
        val enemyName = EnemyName.newBuilder().setValue(enemy.name).build()
        return Enemy.newBuilder()
            .setName(enemyName)
            .setHpMin(enemy.hpMin)
            .setHpMax(enemy.hpMax)
            .setStrengthMin(enemy.strengthMin)
            .setStrengthMax(enemy.strengthMax)
            .setLevelMin(enemy.levelMin)
            .setLevelMax(enemy.levelMax)
            .build()
    }
}


object EnemyRecipeFactory {
    fun create(enemy: Enemy): EnemyRecipe{
        return EnemyRecipe(name = enemy.name.value, hpMin = enemy.hpMin, hpMax = enemy.hpMax, strengthMin = enemy.strengthMin, strengthMax = enemy.strengthMax, levelMin = enemy.levelMin, levelMax = enemy.levelMax)
    }
}