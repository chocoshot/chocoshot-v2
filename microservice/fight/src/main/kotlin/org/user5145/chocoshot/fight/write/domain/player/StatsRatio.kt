package org.user5145.chocoshot.fight.write.domain.player

data class StatsRatio (
    val hp: Float,
    val strength: Float
)