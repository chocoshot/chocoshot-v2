package org.user5145.chocoshot.fight

import org.user5145.chocoshot.amqp.kafka.EventNotMappedException
import org.user5145.chocoshot.amqp.kafka.ServerId
import org.user5145.chocoshot.amqp.kafka.Subscribe
import org.user5145.chocoshot.fight.write.data.EnemyRepo
import org.user5145.chocoshot.fight.write.data.PlayerRepo
import org.user5145.chocoshot.fight.write.domain.player.PlayerRecipeFactory
import org.user5145.chocoshot.fight.write.event.BlockActionCommand
import org.user5145.chocoshot.fight.write.event.CreatedChampionCommand
import org.user5145.chocoshot.shared.event.account.RegistrationTopicIds

class EventHandler (
    val playerRepo: PlayerRepo = PlayerRepo(),
    val enemyRepo: EnemyRepo = EnemyRepo(),
    val createdChampionCommand: CreatedChampionCommand = CreatedChampionCommand,
    val blockActionCommand: BlockActionCommand = BlockActionCommand,
    val factory: PlayerRecipeFactory = PlayerRecipeFactory
    ){

    fun start(){
        addChampionCommand()
        steppedOnBlockCommand()
    }

    private fun addChampionCommand()  {
        Subscribe().subscribe(ServerId.FIGHT.name, RegistrationTopicIds.CREATED_CHAMPION.name) {
            when (it.key()) {
                "1.0" -> { createdChampionCommand.createChampion(it.value(), playerRepo, factory) }
                else -> { throw EventNotMappedException() }
            }
        }
    }

    private fun steppedOnBlockCommand()  {
        Subscribe().subscribe(ServerId.FIGHT.name, RegistrationTopicIds.STEPPED_ON_BLOCK.name) {
            when (it.key()) {
                "1.1" -> { blockActionCommand.executeAction(it.value(), playerRepo, enemyRepo) }
                "1.0" -> {}
                else -> { throw EventNotMappedException() }
            }
        }
    }
}