package org.user5145.chocoshot.fight.write.domain.player

object PlayerRecipeFactory {
    fun create(name: String): PlayerRecipe {
        return PlayerRecipe(name = name)
    }
}