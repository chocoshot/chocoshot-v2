package org.user5145.chocoshot.fight.write.domain.player

import com.fasterxml.jackson.annotation.JsonIgnore
import com.mongodb.client.model.Filters
import com.mongodb.client.model.Filters.gte
import com.mongodb.client.model.Filters.lte
import org.bson.types.ObjectId
import org.user5145.chocoshot.fight.write.data.EnemyRepo
import org.user5145.chocoshot.fight.write.domain.FightSimulatorService
import org.user5145.chocoshot.fight.write.domain.enemy.EnemyRecipe
import kotlin.math.roundToInt
import kotlin.reflect.KProperty1

data class PlayerRecipe (
    val _id: ObjectId = ObjectId(),
    var name: String,
    var experience: Long = 0,
    val history: MutableList<Fight> = ArrayList(),
    var stats: StatsRatio = StatsRatio(0.5f, 0.5f),
    var level: Int = 1,
) {
    fun attack(enemyRepo: EnemyRepo, fightService: FightSimulatorService) {
        val enemy = enemyRepo.getAny(Filters.and( lte(EnemyRecipe::levelMin.name, level), gte(EnemyRecipe::levelMin.name, level)))
        val fight = fightService.simulateFight(enemy, this)
        addHistory(fight)
    }

    fun changeStats(strength: Float, hp: Float){
        if ( (strength + hp) != 1f )
            throw WrongParameters()
        stats = StatsRatio(hp, strength)
    }

    @JsonIgnore
    fun getLatestHistory(): List<Fight> {
        return history.asReversed().take(10)
    }

    fun addExperience(value: Int){
        experience += value
        level = (experience / 1000.0).toInt() + 1
    }

    private fun addHistory(fight: Fight) {
        history.add(fight)
    }

    @JsonIgnore
    fun getStat(kProperty1: KProperty1<StatsRatio, Float>): Int {
        return (kProperty1.get(stats) * level * 100).roundToInt()
    }

    fun createPlayer(): Player {
        return Player(getStat(StatsRatio::hp), getStat(StatsRatio::strength), )
    }
}
