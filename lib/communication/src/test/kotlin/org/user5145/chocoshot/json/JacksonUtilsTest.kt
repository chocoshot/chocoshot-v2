package org.user5145.chocoshot.json

import org.junit.Assert
import org.user5145.chocoshot.json.extension.jsonToObject
import org.user5145.chocoshot.json.extension.toJson
import kotlin.test.Test

data class Login (val value: String)

class JacksonUtilsTest {

    @Test
    fun testJsonDeserialization(){
        //given
        val json = """{"value":"login"}""";

        //when
        val login: Login = json.jsonToObject()

        //then
        Assert.assertEquals("login", login.value)
    }

    @Test
    fun testJsonSerialization(){
        //given
        val login = Login("login");

        //when
        val json = login.toJson()

        //then
        Assert.assertEquals("login", login.value)
    }
}