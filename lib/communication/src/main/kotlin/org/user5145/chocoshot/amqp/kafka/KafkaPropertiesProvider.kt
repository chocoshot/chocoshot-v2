package org.user5145.chocoshot.amqp.kafka

import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kafka.common.serialization.StringSerializer
import org.user5145.chocoshot.properties.ConfigContext
import java.util.*

object KafkaPropertiesProvider {
    var servers = ConfigContext.kafkaServers
    var username = ConfigContext.kafkaUsername
    var password = ConfigContext.kafkaPassword
    var securityProtocol = "SASL_SSL"

    fun getProperties(id: String): Properties {
        val jaasCfg =
            "org.apache.kafka.common.security.scram.ScramLoginModule required username=\"$username\" password=\"$password\";"

        val serializer = StringSerializer::class.qualifiedName
        val deserializer = StringDeserializer::class.qualifiedName
        val props = Properties()
        props.put("bootstrap.servers", servers)
        props.put("group.id", "$id-consumer")
        props.put("enable.auto.commit", "true")
        props.put("auto.commit.interval.ms", "500")
        props.put("auto.offset.reset", "earliest")
        props.put("session.timeout.ms", "20000")
        props.put("key.deserializer", deserializer)
        props.put("value.deserializer", deserializer)
        props.put("key.serializer", serializer)
        props.put("value.serializer", serializer)
        props.put("security.protocol", securityProtocol)
        props.put("sasl.mechanism", "SCRAM-SHA-256")
        props.put("sasl.jaas.config", jaasCfg)
        return props
    }
}
