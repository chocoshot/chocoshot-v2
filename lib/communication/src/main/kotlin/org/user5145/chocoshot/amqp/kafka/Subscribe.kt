package org.user5145.chocoshot.amqp.kafka

import mu.KLogger
import mu.KotlinLogging
import org.apache.kafka.clients.consumer.Consumer
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.user5145.chocoshot.properties.ConfigContext
import java.util.*
import kotlin.concurrent.thread


class Subscribe(
    private val topicPrefix: String = ConfigContext.kafkaPrefix
){
    private val logger:KLogger = KotlinLogging.logger {}

    fun subscribe(id: String, topic: String, forEach:(ConsumerRecord<String, String>) -> Unit): Consumer<String, String> {
        // Create the consumer using props.
        val consumer: Consumer<String, String> = KafkaConsumer(KafkaPropertiesProvider.getProperties(id))

        // Subscribe to the topic.
        consumer.subscribe(Collections.singletonList("$topicPrefix$topic"))
        runConsumer(consumer, forEach)
        return consumer
    }

    @Throws(InterruptedException::class)
    private fun <K, V> runConsumer(consumer: Consumer<K, V>, forEach:(ConsumerRecord<K, V>) -> Unit) = thread(true) {
        while (true) {
            val consumerRecords = consumer.poll(1000)
            if (consumerRecords.count() == 0)
                logger.debug { "didn't find any new records" }

            consumerRecords.forEach { forEach(it) }
        }
    }
}
