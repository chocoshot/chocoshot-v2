package org.user5145.chocoshot.amqp.kafka

enum class ServerId() {
    FIGHT,
    REGISTRATION,
    TRAVEL,
}