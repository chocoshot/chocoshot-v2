package org.user5145.chocoshot.json.extension

import org.user5145.chocoshot.json.JacksonUtils
import kotlin.reflect.KClass

fun String.isJsonValid() = JacksonUtils.isValid(this)

fun <T : Any> String.jsonToObject(clazz: KClass<T>): T =
    JacksonUtils.jsonToObject(this, clazz)

inline fun <reified T> String.jsonToObject(): T {
    return JacksonUtils.jsonToObject(this)
}