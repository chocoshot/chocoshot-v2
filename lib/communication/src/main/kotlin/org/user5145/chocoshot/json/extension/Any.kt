package org.user5145.chocoshot.json.extension

import org.user5145.chocoshot.json.JacksonUtils
import kotlin.String

fun <T : Any> T.toJson(): String {
    return JacksonUtils.objectToJson(this)
}