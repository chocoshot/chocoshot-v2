package org.user5145.chocoshot.amqp.kafka

import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerRecord
import org.user5145.chocoshot.properties.ConfigContext


object PushEvent {
    private val topicPrefix = ConfigContext.kafkaPrefix

    fun push(topic: String, key: String, data: String, id: String){
        val producer = KafkaProducer<String, String>(KafkaPropertiesProvider.getProperties(id))
        producer.send(ProducerRecord("$topicPrefix$topic", key , data))
    }
}
