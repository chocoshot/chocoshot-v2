package org.user5145.chocoshot.grpc

import io.grpc.BindableService
import io.grpc.Server
import io.grpc.ServerBuilder
import mu.KotlinLogging

object GrpcServer {
    private val log = KotlinLogging.logger {}
    private val servers: MutableList<Server> = mutableListOf()

    fun start(service: BindableService, port: ServerPort): Thread {
        val thread = Thread {
            blockingStart(service, port)
        }
        thread.start()
        return thread
    }

    fun shutdown(){
        servers.parallelStream().forEach { it.shutdown() }
        servers.clear()
    }

    private fun blockingStart(service: BindableService, port: ServerPort){
        val server = ServerBuilder
            .forPort(port.value)
            .addService(service)
            .build()

        log.info { "starting server at port ${port.value}" }
        server.start()
        log.info { "started the server at port ${port.value}" }
        server.awaitTermination()
    }
}