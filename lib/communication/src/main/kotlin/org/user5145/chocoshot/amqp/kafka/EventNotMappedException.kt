package org.user5145.chocoshot.amqp.kafka

class EventNotMappedException: Exception(){
    override val message: String?
        get() = """
            Event wasn't mapped, system's data stability is at risk
        """.trimIndent()
}