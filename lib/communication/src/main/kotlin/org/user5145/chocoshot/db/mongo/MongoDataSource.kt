package org.user5145.chocoshot.db.mongo

import com.mongodb.ConnectionString
import com.mongodb.MongoClientSettings
import com.mongodb.client.MongoClient
import org.litote.kmongo.KMongo
import org.user5145.chocoshot.properties.ConfigContext
import java.util.concurrent.TimeUnit

object MongoDataSource {
    var url = ConnectionString(ConfigContext.mongoUrl)
    lateinit var mongoClient: MongoClient
        private set

    init {
        reloadMongoSettings()
    }

    fun reloadMongoSettings() {
        val settings = MongoClientSettings.builder()
            .applyConnectionString(url)
            .applyToSocketSettings { it.connectTimeout( 300, TimeUnit.SECONDS) }
            .build()
        mongoClient = KMongo.createClient(settings)
    }
}
