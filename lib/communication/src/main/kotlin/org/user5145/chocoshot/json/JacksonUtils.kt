package org.user5145.chocoshot.json

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility
import com.fasterxml.jackson.annotation.PropertyAccessor
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import kotlin.reflect.KClass

object JacksonUtils {
    const val jsonNull = "null"

    val mapper = ObjectMapper()

    init {
        mapper.registerKotlinModule()
        mapper.setVisibility(PropertyAccessor.ALL, Visibility.NONE)
        mapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY)
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        mapper.setVisibility(PropertyAccessor.GETTER, Visibility.NONE)
        mapper.setVisibility(PropertyAccessor.IS_GETTER, Visibility.NONE)
        mapper.setVisibility(PropertyAccessor.SETTER, Visibility.NONE)
    }

    internal fun <T : Any> jsonToObject(json: String, clazz: KClass<T>) = mapper.readValue(json, clazz.javaObjectType)

    inline fun <reified T> jsonToObject(json: String): T = mapper.readValue(json, object : TypeReference<T>() {})

    internal fun objectToJson(value: Any) = mapper.writeValueAsString(value)

    internal fun isValid(json: String): Boolean = runCatching { mapper.readTree(json) }.isSuccess
}


