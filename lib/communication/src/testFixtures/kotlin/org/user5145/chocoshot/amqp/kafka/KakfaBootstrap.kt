package org.user5145.chocoshot.amqp.kafka

import org.testcontainers.containers.KafkaContainer
import org.testcontainers.containers.Network
import org.testcontainers.utility.DockerImageName

object KakfaBootstrap {
    fun setupKafka(): KafkaContainer{
        val kafka = createKafkaContainer()
        configurePorts(kafka)
        return kafka
    }

    private fun createKafkaContainer(): KafkaContainer {
        val kafka = KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:5.4.3"));
        kafka.start()
        return kafka
    }

    private fun configurePorts(kafka: KafkaContainer) {
        KafkaPropertiesProvider.servers = kafka.bootstrapServers
        KafkaPropertiesProvider.securityProtocol = "PLAINTEXT"
    }
}
