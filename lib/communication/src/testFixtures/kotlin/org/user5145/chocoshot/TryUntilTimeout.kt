package org.user5145.chocoshot

import java.util.*
import java.util.concurrent.TimeoutException

class TryUntilTimeout {
    companion object {
        private val timeOutMiliSeconds = 3 * 1000

        fun <T> execUntilTimeout(action: () -> T): T? {
            val startupTime = Calendar.getInstance().timeInMillis
            do {
                kotlin.runCatching {
                    return action()
                }
                val now = Calendar.getInstance().timeInMillis
            } while (startupTime > now - timeOutMiliSeconds)
            throw TimeoutException()
        }
    }
}