package org.user5145.chocoshot.mongo

import com.mongodb.ConnectionString
import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.MongoDBContainer
import org.testcontainers.utility.DockerImageName
import org.user5145.chocoshot.db.mongo.MongoDataSource

object MongoContainerFactory {
    fun create(): MongoDBContainer {
        val container = MongoDBContainer(DockerImageName.parse("mongo:4.0.10"))
        container.start()
        MongoDataSource.url = ConnectionString(container.replicaSetUrl)
        MongoDataSource.reloadMongoSettings()
        return container
    }
}
