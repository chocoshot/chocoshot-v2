package org.user5145.chocoshot

import java.util.*
import kotlin.collections.HashMap
import kotlin.test.assertTrue
import java.util.concurrent.TimeoutException

class TimedOutAssert {
    companion object {
        private val timeOutMiliSeconds = 3 * 1000

        fun assertTrueBeforeTimeout(comparator: () -> Boolean){
            timedOutAssert{ assertTrue(comparator())}
        }

        private fun timedOutAssert(assert: () -> Unit) {
            val startupTime = Calendar.getInstance().timeInMillis
            do {
                kotlin.runCatching {
                    return assert()
                }
                val now = Calendar.getInstance().timeInMillis
            } while (startupTime > now - timeOutMiliSeconds)
            return
        }
    }
}