package org.user5145.chocoshot.properties

import com.typesafe.config.ConfigFactory

object ConfigContext {
    private val config = ConfigFactory.load()

    //region server
    val defaultServer = config.getInt("server.default.id")
    //endregion

    //region kafka
    val kafkaServers = config.getString("kafka.servers")
    val kafkaUsername = config.getString("kafka.username")
    val kafkaPassword = config.getString("kafka.password")
    val kafkaPrefix = config.getString("kafka.topic.prefix")
    //endregion

    //region mongodb
    val mongoUrl = config.getString("mongo.url")
    //endregion
}