# ChocoShot 2

# tweaks that may be needed (Intellij IDEA) 
- Build, Execution, Deployment -> Build Tools -> Gradle -> Gradle JVM -> 11
- Build, Execution, Deployment -> Compiler -> Java Compiler -> project bytecode version -> 11
- Build, Execution, Deployment -> Compiler -> Kotlin Compiler -> enable incremental compilation -> on
- Build, Execution, Deployment -> Compiler -> Kotlin Compiler -> Target JVM version -> 11
- Build, Execution, Deployment -> Compiler -> Kotlin Compiler -> enable new type interference algorithm for IDE analysis -> on
- Build, Execution, Deployment -> Debugger -> Force Classic VM for JDK 1.3 and earlier -> off
- Project structure -> Project SDK -> Java 11


# Requirements 
### Client
- internet connection 1Mb/s+
- cpu i3-2365M or faster
- 2gb ram

### Server (increased by amount of users)
- Ram: 500MB+
- Cpu like aws t2.nano or better
- mongo
- kafka
